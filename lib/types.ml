open Unspec
open Skeltypes
module SM = Statemonad
type 'a set = 'a list
type 'a tuple = 'a list

(* From base types and operations on base types, builds abstract values and operations on abstract values (comparison, union...) *)
module BuildTypes (Unspec: UNSPEC) = struct
  module SMap = Util.SMap
  include Unspec

  (* Type of program points *)
  type ppoint = Ppoint.t

  (* Base values *)
  type base_value = abst_value Unspec.base_value

  (* Abstract values *)
  and abst_value =
    | Tuple of abst_value tuple set
    | Fun of abst_function set
    | Const of constructor * abst_value
    | Base of base_value
    | PPoint of ppoint
    | Bot
    | Top

  (* The constructor for the different functions *)
  and abst_function =
    | SpecFun of int * abst_value list * string * abst_value
    | UnspecFun of int * abst_value list * string
    | Clos of pattern * env * skeleton

  (* The abstract environment, mapping skeletal variables to abstract values *)
  and env = (string * abst_value) list

  (* The state of the abstract interpretation *)
  type ai_state = abst_value Unspec.ai_state

  (* Comparison functions for abstract values *)
  let rec lesser (ais: ai_state) (v1: abst_value) (v2: abst_value): bool =
    match (v1, v2) with
    | (Tuple l1s, Tuple l2s) ->
      (* Comparison of two tuples (v₁#,.., vₙ#) ≤ (w₁#,..,wₙ#) iff vᵢ# ≤ wᵢ# *)
      let lesser_t l1 l2 =
        List.for_all2 (lesser ais) l1 l2 in

      (* l1s ≤ l2s iff for all l1 ∈ l1s, ∃ l2 ∈ l2s such that l1 ≤# l2 *)
      Set.for_all (fun l1 -> Set.exists (lesser_t l1) l2s) l1s
    | (Fun _, Fun _) ->
      failwith "lesser: functions not comparable"
    | (Const(c1, v1), Const(c2, v2)) when c1 = c2 ->
      (* C(v₁#) ≤ C(v₂#) iff v₁# ≤ v₂# *)
      lesser ais v1 v2
    | (Const(c1, _), Const(c2, _)) when c1 <> c2 -> false
    | (PPoint pp1, PPoint pp2) -> pp1 = pp2
    | (Base bv1, Base bv2) -> lesser_base_value ais bv1 bv2
    | (Bot, _) -> true (* ∀ v#, ⊥ ≤ v# *)
    | (_, Top) -> true (* ∀ v#, v# ≤ ⊤ *)
    | (_, Bot) -> false (* ∀ v# ≠ ⊥, v# ≰ ⊥*)
    | (Top, _) -> false (* ∀ v# ≠ ⊤, ⊤ ≰ v#*)
    |  _ -> false
  and lesser_base_value ais bv1 bv2 = Unspec.lesser_base_value ais (lesser ais) bv1 bv2

  (* Equality on abstract values *)
  let equal_abst ais v1 v2 = lesser ais v1 v2 && lesser ais v2 v1

  let equal_abst_list ais t1 t2 = List.for_all2 (equal_abst ais) t1 t2

  (* Printing functions for abstract values and base values *)
  let rec av_to_string (ais: ai_state) = function
    | Tuple vl_set ->
      let tuple_to_string (vl: abst_value list) =
        Printf.sprintf "(%s)" (String.concat ", " (List.map (av_to_string ais) vl)) in
      Printf.sprintf "{ %s }" (String.concat ", " (List.map tuple_to_string vl_set))
    | Fun fl ->
      let fun_to_string (f: abst_function) =
        match f with
        | SpecFun(_, vl, fname, _) | UnspecFun(_, vl, fname) ->
          "function: " ^ fname ^ " " ^
          String.concat " " (List.map (av_to_string ais) vl)
        | Clos(p, _, s) ->
          (Necro.Printer.string_of_pattern p) ^ " -> " ^ (Necro.Printer.string_of_skeleton ~indent:0 [] s) in
      String.concat "\n" (List.map fun_to_string fl)
    | Const((_, cname, _), v) ->
      cname ^ "(" ^ (av_to_string ais v) ^ ")"
    | Base bv -> bv_to_string ais bv
    | PPoint pp ->
      Ppoint.to_string pp
    | Bot -> "⊥"
    | Top -> "⊤"
  and bv_to_string ais bv = Unspec.bv_to_string ais (av_to_string ais) bv

  (* Printing function for the abstract interpretation state *)
  let string_of_ai_state (ais: ai_state): string = Unspec.string_of_ai_state ais (av_to_string ais)

  (* Join function for abstract values. Given a widening of base values, this function widens abstract values *)
  let rec join_f (f: base_value -> base_value -> (ai_state, base_value) SM.t) (v1: abst_value) (v2: abst_value): (ai_state, abst_value) SM.t =
    (fun (ais: ai_state) ->
       match (v1, v2) with
       | (Tuple l1s, Tuple l2s) ->
         (ais, Tuple (Set.union (equal_abst_list ais) l1s l2s))
       | (Fun f1, Fun f2) -> ais, Fun (f1 @ f2)
       | (Const(c1, v1), Const(c2, v2)) when c1 = c2 ->
         let new_ais, v = join_f f v1 v2 ais in
         new_ais, Const(c1, v)
       | (Const(c1, _), Const(c2, _)) when c1 <> c2 -> ais, Top
       | (Base bv1, Base bv2) ->
         let new_ais, bv = f bv1 bv2 ais in
         new_ais, Base bv
       | (Bot, v) | (v, Bot) -> ais, v
       | (Top, _) | (_, Top) -> ais, Top
       |  _ -> failwith "join: abstract values not joinable"
    )

  (* Join and widen functions for base values *)
  let rec join_base_value bv1 bv2 = Unspec.join_base_value (join_f join_base_value) bv1 bv2

  let rec widen_base_value bv1 bv2 = Unspec.widen_base_value (join_f widen_base_value) bv1 bv2

  (* The mapping containing the unspecified functions *)
  let unspec_funs: (abst_value list -> (ai_state, abst_value) SM.t) SMap.t = SMap.empty

  (* The mapping containing the unspecified values *)
  let unspec_vals: abst_value SMap.t = SMap.empty

  (* The set of types that must be replaced with program-points *)
  let pp_type_set: necro_type list = []
  let get_prg (ais: ai_state) = Unspec.get_prg ais

  (* Access to a sub-term of the main program *)
  let get_subterm_of_prg (ais: ai_state) (pp: Ppoint.ppoint): abst_value =

    (* Get a subterm of an abstract value given an int list representing a ppoint *)
    let rec get_subterm (term: abst_value) (pp: int list): abst_value =
      match pp with
      | [] -> term
      | i :: pp' ->
         begin
           match term with
           | Const(_, Tuple([tup])) ->
              let subterm = List.nth tup i in
              get_subterm subterm pp'
           | Const(_, arg) when i = 0 ->
              arg
           | _ ->
              failwith "get_subterm: term is expected to be a constant" 
         end
    in

    match pp with
    | SubTerm(_, pp_raw) ->
       get_subterm (get_prg ais) pp_raw

  (* The skeletal semantics *)
  let ss = Unspec.ss

  (* Default update functions *)
  let update_in (_: fun_name) (params: abst_value list): (ai_state, abst_value list) SM.t =
    SM.return params
  let update_out (_: fun_name) (_: abst_value list) (res: abst_value) =
    SM.return res

  (** Get the input type of a constructor *)
  let get_input_type_const ((_, const_name, out_nt): constructor): necro_type =
    (* Retrieves the list of specified types in the skeletal semantics *)
    let ltypes = List.filter_map
                   (fun (tname, (_, nt, signlo)) ->
                     match signlo with
                     | Some sign -> Some (tname, nt, sign)
                     | None -> None) (SMap.bindings ss.ss_types) in

    (* Keeps only the types equal to the output type of the constructor *)
    let ltypes = List.filter (fun (_, nt, _) -> nt = out_nt) ltypes in

    (* Given a type list, return the input type of the constructor *)
    let rec find_type_rec ltypes =
      match ltypes with
      | (_, _, signl) :: tl ->
         (* Searched for the constructor in the constructor list of the type (_, _, signl) *)
         let signo = List.find_opt (fun sign -> sign.s_name = const_name) signl in
         begin
           match signo with
           | Some sign -> sign.s_input_type (* Returns the input type *)
           | None -> find_type_rec tl
         end
      | _ -> failwith "get_input_type_const: not a one element list" in

    find_type_rec ltypes

  (** [get_pp pp v] returns [v] @ [pp]: the subvalue of [v] at program-point [pp] *)
  let rec get_pp (pp: int list) (v: abst_value): abst_value =
    match (pp, v) with
    | ([], _) -> v
    | (i :: pp', Const(_, Tuple(vls))) -> get_pp pp' (List.nth (List.hd vls) i) (* TODO: how to represent programs? *)
    | (0 :: pp', Const(_, v)) -> get_pp pp' v
    | _ -> failwith "get_pp"

  (** [unfold ais pp v] Unfolds a program point, given a program-point [pp],
      returns [v]@[pp], where values of types in pp_type_set are replaced with ppoint
   *)
  let unfold (pp_type_set: necro_type list) (pp: ppoint) (v: abst_value): abst_value =
    (* We retrieve v @ pp *)
    let vpp =
      match pp with
      | SubTerm (_, l) -> get_pp l v
    in
    (* v @ pp should be a Const(const, v) , we retrieve the constructor and the abstract value *)
    let ((from, cname, nt), v') =
      match vpp with
      | Const(const, v) -> (const, v)
      | _ ->  failwith "unfold: not a Const" in

    match (v', get_input_type_const (from, cname, nt)) with
    | (Tuple(vls), Product(tyl)) ->
       (* Auxiliary function that unfolds a tuple of abstract values *)
       let unfold_tuple vl =
         (* List of pairs (value, type) *)
         let vtyl = List.combine vl tyl in
         let pp_vl = List.mapi
                       (fun i (v, ty) ->
                         (* If the type of the value v is in pp_type_set, v is replaced with its program point *)
                         if (List.mem ty pp_type_set) then PPoint(Ppoint.add pp [i])
                         else v) vtyl in
         pp_vl in

       (* Unfold all the tuples in the tuple set vls *)
       Const((from, cname, nt), Tuple(List.map unfold_tuple vls))
    | (v', ntyv') ->
       (* If the type of v' is in pp_type_set, v', is replaced by its program point *)
       let new_v' =
         if List.mem ntyv' pp_type_set then PPoint(Ppoint.add pp [0])
         else v' in
       let new_v =
         Const((from, cname, nt), new_v') in
       new_v
end
