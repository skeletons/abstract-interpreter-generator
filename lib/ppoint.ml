type flowpp =
  | In
  | Out
type ppoint =
  | SubTerm of flowpp * int list

type t = ppoint

let compare_flowpp f1 f2 =
  match (f1, f2) with
  | (In, Out) -> 1
  | (Out, In) -> -1
  | _ -> 0

let compare (pp1: ppoint) (pp2: ppoint): int =
  match (pp1, pp2) with
  | SubTerm(f1, l1), SubTerm(f2, l2) ->
    let fdiff = compare_flowpp f1 f2 in
    if fdiff = 0 then Stdlib.compare l1 l2
    else fdiff

let flowpp_to_string (f: flowpp) =
  match f with
  | In -> "ᵢₙ"
  | Out -> "ₒᵤₜ"

let to_string (pp: ppoint): string =
  match pp with
  | SubTerm(f, pp_st) ->
    begin
      match pp_st with
      | [] -> "epsi" ^ (flowpp_to_string f)
      | _ -> String.concat "" (List.map Int.to_string pp_st) ^ (flowpp_to_string f)
    end

let epsilon_in: ppoint = SubTerm(In, [])
let epsilon_out: ppoint = SubTerm(Out, [])
let epsilon: ppoint = SubTerm(In, [])

let is_prefix (pp1: ppoint) (pp2: ppoint): bool =
  match (pp1, pp2) with
  | SubTerm(f1, l1), SubTerm(f2, l2) when f1 = f2 -> Help.is_prefix l1 l2
  | _ -> false

let add (pp1: ppoint) (il: int list): ppoint =
  match pp1 with
  | SubTerm (f, l1) -> SubTerm (f, l1@il)

let rec remove_last (l: 'a list): 'a list =
  match l with
  | [] -> []
  | [ _ ] -> []
  | a :: tl -> a :: remove_last tl

let parent (pp: ppoint): ppoint =
  match pp with
  | SubTerm (_, []) -> failwith "PPoint.parent: not parent"
  | SubTerm (f, pp) -> SubTerm(f, remove_last pp)
