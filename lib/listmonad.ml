(* The list monad *)

let ret (x: 'a): 'a list = [ x ]

let bind (mx: 'a list) (f: 'a -> 'b list): 'b list =
  List.flatten (List.map f mx)
