(** Module that defines the State Monad *)

(* the monad type *)
type ('s, 'a) t = 's -> ('s * 'a)

(* return operator *)
let return (x: 'a): ('s, 'a) t =
  fun s -> (s, x)

(* binding operator *)
let binding (x: ('s, 'a) t) (f: 'a -> ('s, 'b) t): ('s, 'b) t =
  (fun s ->
     let (s', x') = x s in
     let cont = f x' in
     cont s'
  )

(* retrieve the state *)
let get (): ('s, 's) t =
  (fun s -> (s, s))

(* override the state *)
let put (s: 's): ('s, unit) t =
  (fun _ -> (s, ()))

(* monadic fold for lists *)
let rec foldm_list (f: 'a -> 'b -> ('s, 'a) t) (acc: 'a) (l:'b list): ('s, 'a) t =
  match l with
  | [] -> return acc
  | head :: tail ->
     binding (f acc head) (fun acc' -> foldm_list f acc' tail)

(* monadic fold for sets *)
let foldm_set (f: 'a -> 'b -> ('s, 'a) t) (acc: 'a) (s: 'b Set.t): ('s, 'a) t =
  foldm_list f acc s
