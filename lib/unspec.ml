open Skeltypes
module SM = Statemonad

type nb_remaining_args = int
type fun_name = string

(** The UNSPEC module signature with everything needed to defined the unspecified terms and types of a skeletal semantics *)
module type UNSPEC =
sig
  (* The values of the unspecified types *)
  type 'abst base_value

  (* The state of the abstract interpretation *)
  type 'abst ai_state

  val init_ai_state: 'abst -> 'abst ai_state
  val string_of_ai_state: 'abst ai_state -> ('abst -> string) -> string

  (* Comparison functions *)
  val lesser_base_value: 'abst ai_state -> ('abst -> 'abst -> bool) -> 'abst base_value -> 'abst base_value -> bool

  (* Abstract union functions *)
  val join_base_value: ('abst -> 'abst -> ('abst ai_state, 'abst) SM.t) -> 'abst base_value -> 'abst base_value -> ('abst ai_state, 'abst base_value) SM.t
  val widen_base_value: ('abst -> 'abst -> ('abst ai_state, 'abst) SM.t) -> 'abst base_value -> 'abst base_value -> ('abst ai_state, 'abst base_value) SM.t

  (* Printing function *)
  val bv_to_string: 'abst ai_state -> ('abst -> string) -> 'abst base_value -> string

  (* The function that retrieves the current program being evaluated *)
  val get_prg: 'abst ai_state -> 'abst

  (* The Skeletal Semantics *)
  val ss: Skeltypes.skeletal_semantics
end


module type TYPES = sig
  module SMap = Util.SMap

  (* The type of basic values *)
  type base_value

  (* The type of program-points *)
  type ppoint = Ppoint.t

  (* The abstract values, that are very similar to normal skeleton values *)
  type abst_value =
    | Tuple of abst_value list list
    | Fun of abst_function list
    | Const of constructor * abst_value (* TODO: maybe use constructor type? Const of constructor * type * value *)
    | Base of base_value
    | PPoint of ppoint
    | Bot
    | Top

  (* The constructor for the different functions *)
  and abst_function =
    | SpecFun of nb_remaining_args * abst_value list * fun_name * abst_value
    | UnspecFun of nb_remaining_args * abst_value list * fun_name
    | Clos of pattern * env * skeleton

  (* The abstract environment *)
  and env = (string * abst_value) list

  (* The state of the interpretation *)
  type ai_state

  val init_ai_state: abst_value -> ai_state
  val string_of_ai_state: ai_state  -> string

  (* Printer for base values and abstract values *)
  val bv_to_string: ai_state -> base_value -> string
  val av_to_string: ai_state -> abst_value -> string

  (* The join functions for base and abstract values, and a widening operator on base values*)
  val join_f: (base_value -> base_value -> (ai_state, base_value) SM.t) -> abst_value -> abst_value -> (ai_state, abst_value) SM.t
  val join_base_value: base_value -> base_value -> (ai_state, base_value) SM.t
  val widen_base_value: base_value -> base_value -> (ai_state, base_value) SM.t

  (* Functions to test if an abstract value is included in another abstract value *)
  val lesser: ai_state -> abst_value -> abst_value -> bool
  val lesser_base_value: ai_state -> base_value -> base_value -> bool
  val equal_abst: ai_state -> abst_value -> abst_value -> bool

  (* The mapping of with the unspecified functions and values *)
  val unspec_vals: abst_value SMap.t
  val unspec_funs: (abst_value list -> (ai_state, abst_value) SM.t) SMap.t

  (* The set of types that can be replaced with program-points, and the function to get the program *)
  val pp_type_set: necro_type list
  val get_prg: ai_state -> abst_value


  (* The skeletal semantics *)
  val ss: Skeltypes.skeletal_semantics

  (* Updates functions *)
  val update_in: fun_name -> abst_value list -> (ai_state, abst_value list) SM.t
  val update_out: fun_name -> abst_value list -> abst_value -> (ai_state, abst_value) SM.t

  (* Unfold functions *)
  val get_input_type_const: constructor -> necro_type
  val get_pp: int list -> abst_value -> abst_value
  val unfold: necro_type list -> ppoint -> abst_value -> abst_value
end
