open Necro
open Skeltypes

module SMap = Util.SMap

(** Get the input type of a constructor *)
let get_input_type_const (ss: skeletal_semantics)((_, cname, out_nt): constructor): necro_type =
  let ltypes = List.filter_map (fun (tname, (_, nt, signlo)) -> match signlo with | Some sign -> Some (tname, nt, sign) | None -> None) (SMap.bindings ss.ss_types) in
  let ltypes = List.filter (fun (_, nt, _) -> nt = out_nt) ltypes in

  let rec find_type_rec ltypes =
    match ltypes with
    | (_, _, signl) :: tl ->
      let signo = List.find_opt (fun sign -> sign.s_name = cname) signl in
      begin
        match signo with
        | Some sign -> sign.s_input_type
        | None -> find_type_rec tl
      end
    | _ -> failwith "get_input_type_const: not a one element list" in

  find_type_rec ltypes

(** Returns the arity of a function given its type *)
let rec arity (t: necro_type): int =
  match t with
  | Arrow(_, t') -> 1 + (arity t')
  | _ -> 0

(** Returns two lists, the first one with n elements, the other one with the rest *)
let split_n n l =
  assert (List.length l >= n);
  let first_n = List.filteri (fun i _ -> i < n) l in
  let last_n = List.filteri (fun i _ -> i >= n) l in
  (first_n, last_n)

(** Get a constructor from a skeletal semantics
*)
let get_constr (ss: Skeltypes.skeletal_semantics)(c: string) (t: string): constructor =
  let ty = Util.SMap.find t ss.ss_types in
  match ty with
  | (_, _, Some sigl) ->
    let sigl = List.filter (fun sign -> sign.s_name = c) sigl in
    begin
      match sigl with
      | e :: [] -> (None, c, e.s_output_type)
      | _ -> failwith "get_constr: should be a one element list"
    end
  | _ -> failwith "get_constr: not signature list"

(** Remove duplicates from a list *)
let rec remove_duplicates (equals: 'a -> 'a -> bool) (l: 'a list): 'a list =
  match l with
  | [] -> []
  | hd :: tl ->
    if List.exists (equals hd) tl then (remove_duplicates equals tl)
    else hd :: remove_duplicates equals tl

let rec common_prefix l1 l2 =
  match (l1, l2) with
  | (hd1 :: tl1, hd2 :: tl2) when hd1 = hd2 ->
    hd1 :: (common_prefix tl1 tl2)
  | _ -> []

let rec is_prefix pref l =
  match (pref, l) with
  | ([], _) -> true
  | (hd :: pref' , hd' :: l') when hd = hd' -> is_prefix pref' l'
  | _ -> false

