(** This module defines a functor to define sets without the need of a total order, only the equality is needed *)

type 'a t = 'a list

let mem compare x s = List.exists (compare x) s

let is_empty s = s = []

let add compare v s =
  if mem compare v s then s
  else v :: s

let empty = []

let singleton v = [v]

let remove compare v s =
  List.filter (compare v) s

let rec setify compare l =
  match l with
  | [] -> []
  | hd :: tl ->
    hd :: (setify compare (remove compare hd tl))

let rec union compare s1 s2 =
  match s1 with
  | [] -> s2
  | v :: s1' ->
    if mem compare v s2 then
      union compare s1' s2
    else
      v :: (union compare s1' s2)

let rec inter compare s1 s2 =
  match s1 with
  | [] -> []
  | v :: s1' ->
    if mem compare v s2 then
      v :: inter compare s1' s2
    else
      inter compare s1' s2

let disjoint compare s1 s2 = (inter compare s1 s2) = []

let rec diff compare s1 s2 =
  match s2 with
  | [] -> s1
  | v :: s2' ->
    diff compare (remove compare v s1) s2'

let exists = List.exists

let for_all = List.for_all

let filter = List.filter

let fold = List.fold_left

let cardinal = List.length

let rec subset compare s1 s2 =
  match s1 with
  | [] -> true
  | e :: s1' ->
    mem compare e s2 && subset compare s1' s2

let equal compare s1 s2 =
  cardinal s1 = cardinal s2
  && subset compare s1 s2

let to_string elt_to_string s =
  "{ " ^ (String.concat ", " (List.map elt_to_string s)) ^ " }"

module SpecializeSet(C: sig type t val compare: t -> t -> bool end) = struct
  type t = C.t
  let mem = mem C.compare

  let is_empty: t list -> bool = is_empty

  let add = add C.compare

  let empty: t list = empty

  let singleton: t -> t list = singleton

  let remove: t -> t list -> t list = remove C.compare

  let union = union C.compare

  let inter = inter C.compare

  let disjoint = disjoint C.compare

  let diff = diff C.compare

  let exists: (t -> bool) -> t list -> bool = exists

  let for_all: (t -> bool) -> t list -> bool = for_all

  let filter: (t -> bool) -> t list -> t list = filter

  let fold: (t -> t -> t) -> t -> t list -> t = fold

  let cardinal: t list -> int = cardinal

  let subset: t list -> t list -> bool = subset C.compare

  let equal: t list -> t list -> bool = equal C.compare

  let to_string: (t -> string) -> t list -> string = to_string
end
