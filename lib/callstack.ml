module type ABSTCOMP = sig
  (* The Abstract values *)
  type t
  type ai_state

  (* Test for equality *)
  val equal: t -> t -> bool

  (* Printing function *)
  val to_string: ai_state -> t -> string
end

module Make (AV: ABSTCOMP) = struct
  let equal ((fn1, vl1), vo1) ((fn2, vl2), vo2) =
    fn1 = fn2
    && List.for_all2 AV.equal vl1 vl2
    && Option.equal AV.equal vo1 vo2

  type call = (string * AV.t list)
  type t = call list

  (* Check is a callstack is empty *)
  let is_empty (cs: t) = cs = []

  (* Pop the first call, return the tail of the callstack *)
  let pop (cs: t): call * t =
    (List.hd cs, List.tl cs)

  (* Push a call onto the callstack *)
  let push (c: call) (cs: t) =
    c :: cs

  (* Query and printing functions for call stack *)
  let present (calls: t) (fname: string) (vl: AV.t list): bool =
    List.exists (fun (fn, args) -> String.compare fn fname = 0 && List.for_all2 AV.equal vl args) calls

  let to_string (ais: 'abst) (calls: t): string =
    let calls_strings = List.map
        (fun (fn, vl) ->
           let vl_string = String.concat ", " (List.map (AV.to_string ais) vl) in
           Printf.sprintf "%s(%s)" fn vl_string
        ) calls in
    String.concat ";\n" calls_strings
end
