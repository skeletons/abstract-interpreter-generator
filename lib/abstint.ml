open Necro
open Skeltypes
open Unspec
module SMap = Util.SMap
module LM = Listmonad
module SM = Statemonad

(** The AbstInt module defines the functor AbstractInterp *)

(** The AbstractInterp functor takes a TYPES module and returns an abstract interpreter *)
module AbstractInterp (T: TYPES) = struct
  include T

  (** The Calls module is a callstack, used for loop detection *)
  module Calls = Callstack.Make(struct
                     type t = abst_value
                     type ai_state = T.ai_state
                     let equal = Stdlib.(=) (* TODO: Does it always work? What if base values are maps? *)
                     let to_string = av_to_string
                   end)

  (** The AbstState module defines the abstract interpretation state. It contains the abstract interpretation state defined by the user plus the callstack *)
  module AbstState = struct
    type t =
      { ais : ai_state (** The user-defined abstract interpretation state *)
      ;  calls: Calls.t (** The callstack *)
      }

    let to_string (ienv: t): string =
      Printf.sprintf "%s\n%s\n" (T.string_of_ai_state ienv.ais) (Calls.to_string ienv.ais ienv.calls)
  end

  (* Functions to manipulate environments *)

  (** Take a pair (variable, abst_value), an abstract environment and returns a new environment with the new binding **)
  let (+:) (p: (string * abst_value)) (e: env): env =
    p :: e

  (** [assoc_env x e] returns the value associated to x in the environment [e]([x]), or bottom if [e] is bottom *)
  let assoc_env (x: string) (e: env): abst_value =
    List.assoc x e

  (** Comparison of two environment *)
  let equal_env (ais: ai_state) (e1: env) (e2: env): bool =
    (* e1 and e2 are equal if they have the same size and they associate the same key to the same abstract value *)
    List.length e1 = List.length e2 &&
      List.for_all (fun (name, av) ->
          try
            equal_abst ais av (List.assoc name e2)
          with | _ -> false ) e1

  (** [join av1 av2] returns a stateful computation computing av1 ⊔ av2 *)
  let join: abst_value -> abst_value -> ('s, abst_value) SM.t = join_f join_base_value

  (** [join_list vl] returns a stateful computation computing the abstract union of all abstract values of [vl] *)
  let join_list (vl: abst_value list): (ai_state, abst_value) SM.t =
    SM.foldm_list (fun v v' -> join v v') Bot vl

  (* Binding operation for lists *)
  let (>>=) = LM.bind

  (** [patt_match ais e p v] returns a new set of environments
      of the form [e] + [p] → [v] *)
  let rec patt_match (ais: ai_state) (e: env) (p: pattern) (v: abst_value): env Set.t =
    (* patt_match_tuple takes an environment, a pattern list (p₁,..,pₙ),
       and a tuple of abstract values (v₁,..,vₙ),
       and returns the set of environments of the form e[(p₁,..,pₙ) → (v₁,..,vₙ)] *)
    let patt_match_tuple (e: env) (pl: pattern list) (vl: abst_value list) =
      List.fold_left2
        (fun e_set p v ->
          e_set >>= (fun e -> patt_match ais e p v))
        [ e ] pl vl in
    (* (p, v) is matched *)
    match (p, v) with
    | (_, Bot) -> Set.empty
    | (PWild _, _) -> Set.singleton e
    | (PVar(x, _), _) -> Set.singleton ((x, v) +: e)
    | (PConstr(c, _, p), Const(c', v)) when c = c' -> patt_match ais e p v (* The pattern p and the value v must have the same constructor to continue the pattern-matching *)
    | (PConstr(c, _, _), Const(c', _)) when c <> c' -> Set.empty
    | (PConstr(_, _, p), Top) -> patt_match ais e p Top (* The Top value is propagated to the leaves of the value *)
    | (PConstr(_), PPoint(pp)) -> (* When matching a constructor and a ppoint, the ppoint must be 'unfolded' *)
       let prg = get_prg ais in
       patt_match ais e p (unfold pp_type_set pp prg)
    | (PTuple pl, Tuple vls) ->
       (* Compute a new set of environments for each tuple in vls *)
       let e_set_list = (List.map (patt_match_tuple e pl) vls) in

       (* Return the union of all the environment sets *)
       Set.fold (Set.union (equal_env ais)) Set.empty e_set_list
    | (PTuple pl, Top) ->
       let topl = List.init (List.length pl) (fun _ -> Top) in
       patt_match_tuple e pl topl
    | (PRecord _, _) -> failwith "patt_match: records not implemented"
    | _ -> failwith "patt_match: trying to match a pattern and a value that have different patterns"

  (* Monadic pattern matching *)
  let patt_match_mon (e: env) (p: pattern) (v: abst_value): (ai_state, env Set.t) SM.t =
    (fun ais ->
      ais, (patt_match ais e p v)
    )

  (* Monadic pattern matching for sets *)
  let patt_match_set_mon (e_set: env Set.t) (p: pattern) (v: abst_value): (ai_state, env Set.t) SM.t =
    (fun ais ->
      let new_e_set = 
        Set.fold
          (fun e_set e ->
            let e_set' = patt_match ais e p v in
            let new_e_set = Set.union (equal_env ais) e_set e_set' in
            new_e_set
          )
        Set.empty e_set
      in
      ais, new_e_set
    )

  (* let binding for state monad *)
  let ( let* ) = SM.binding

  (** Computes the abstract interpretation of a term
      @param e the abstract environment
      @param t the skeletal term to compute
      @return the result of the abstract interpretation
   *)
  let rec abstract_interpretation_term (e: env) (t: term): abst_value =
    match t with
    | TVar(TVLet(x, _)) -> assoc_env x e
    | TVar(TVTerm(Unspec, _, tname, _, _)) -> SMap.find tname unspec_vals
    | TVar(TVTerm(Spec, _, tname, _, nt)) ->
       (* If a variable corresponds to a specified value, we retrieve its specification *)
       let (_, _, _, topt) = Util.SMap.find tname ss.ss_terms in
       begin
         match topt with
         | Some t ->
            let arity = Help.arity nt in
            let v = abstract_interpretation_term e t in
            (* If the specified values have a function type, a closure is returned *)
            if arity > 0 then
              Fun([SpecFun(Help.arity nt, [], tname, v)])
            else
              (* Otherwise, the values are simply returned*)
              v
         | None ->
            failwith "abstract_interpretation_term: specification of a specified term not found"
       end
    | TConstr(c, _, t) -> Const(c, abstract_interpretation_term e t)
    | TTuple(tl) -> Tuple(Set.singleton (List.map (abstract_interpretation_term e) tl))
    | TFunc(p, s) -> Fun([Clos(p, e, s)])
    | TField(_) | TRecMake(_) |TRecSet(_) -> failwith "abstract_interpretation_term: records not implemented"


  (* [eval_forall_envs calls ais e_set f x] evaluates [f calls ais e x] for all [e]∈ [e_set], the results are joined *)
  let eval_forall_envs (calls: Calls.t) (e_set: env Set.t) (f: Calls.t -> env -> 'a -> (ai_state, abst_value) SM.t) (x: 'a): (ai_state, abst_value) SM.t =
    SM.foldm_set
      (fun (av: abst_value) (e: env) ->
        let* av' = f calls e x in
        join av av'
      ) Bot e_set

  (* [eval_forall_envs calls ais e_set f x] evaluates [f calls ais e x] for all [e]∈ [e_set], the results are joined *)
  let eval_forall_envs_mon (calls: Calls.t) (e_set: env Set.t) (f: Calls.t -> env -> 'a -> (ai_state, abst_value) SM.t) (x: 'a): (ai_state, abst_value) SM.t =
    Set.fold
      (fun (sm_av: (ai_state, abst_value) SM.t) (e: env) ->
        let* av = sm_av in
        let* new_av = f calls e x in
         join av new_av
      ) (SM.return Bot) e_set

  (** Computes the abstract interpretation of a skeleton
      @param calls the current calls stack
      @param e_set set of abstract environment
      @param s the skeleton to compute
      @return a stateful computation performing the abstract interpretation
   *)
  let rec abstract_interpretation_skeleton (calls: Calls.t) (env_set: env Set.t) (s: skeleton): (ai_state, abst_value) SM.t =
    match s with
    | Branching(_, skeleton_list) ->
       SM.foldm_list
         (fun av s ->
           let* av' = abstract_interpretation_skeleton calls env_set s in
           join av av'
         )
       Bot skeleton_list
    | LetIn(_, p, s1, s2) ->
       let* av1  = abstract_interpretation_skeleton calls env_set s1 in
       SM.foldm_set
         (fun v e ->
           let* env_set' = patt_match_mon e p av1 in
           let* v' = abstract_interpretation_skeleton calls env_set' s2 in
           join v v'
         )
         Bot env_set
    | Return(t) ->
       (* Evaluates the term for all the environments *)
       SM.foldm_set
         (fun av e ->
           let av' = abstract_interpretation_term e t in
           join av av'
         ) Bot env_set
    | Apply(t, tl) ->
       (* Evaluates an application for one environment *)
       let eval_app calls e (t, tl) =
         abstract_interpretation_application calls (abstract_interpretation_term e t) (List.map (abstract_interpretation_term e) tl) in
       (* Evaluates the application for all the environments *)
       eval_forall_envs calls env_set eval_app (t, tl)

    | Exists(_) -> failwith "abstract_interpretation_skeleton: exists skeleton not handled"
    | Match(_) -> failwith "abstract_interpretation_skeleton: match skeleton not handled"

  (** Computes the abstract interpretation of a function applied to a list of values
      @param calls the current calls stack
      @param v the abstract value that should be a set of functions
      @param vl the list of arguments of the functions
      @return a stateful computation returning performing the abstract application
   *)
  and abstract_interpretation_application (calls: Calls.t) (v: abst_value) (vl: abst_value list): (ai_state, abst_value) SM.t =
    (* If the arguments list is empty, the application is over*)
    if vl = [] then SM.return v

    (* If one argument is Bottom, Bottom is returned *)
    else if List.mem Bot vl then
      SM.return Bot
    (* Otherwise, we proceed with the application *)
    else
      begin
        match v with
        (* A function is expected *)
        | Fun(fl) ->
           (* We perform the application for all function f ∈ fl *)
           SM.foldm_list
             (fun v f ->
               let* v' = abstract_interpretation_function calls f vl in
               join v' v
             )
             Bot fl
        | _ -> failwith "abstract_interpretation_application: not a function"
      end

  (** Computes the abstract interpretation of a unique function applied to a list of arguments
      @param calls the current calls stack
      @param f the abstract function to apply
      @param vl the list of arguments
      @return a stateful computation performing the call to [f] on parameters in [vl]
   *)
  and abstract_interpretation_function (calls: Calls.t) (f: abst_function) (vl: abst_value list): (ai_state, abst_value) SM.t =
    (* vl = v1 :: vl' *)
    let (v1, vl') =
      match vl with
      | [] -> failwith "abstract_interpretation_function: expected arguments"
      | v :: vl -> (v, vl) in

    match f with
    (* If the function is a closure *)
    | Clos(p, e, s) ->
       (* We interpret the skeleton with in environments p maps to v1 *)
       let* ais = SM.get () in
       let* w1 = abstract_interpretation_skeleton calls (patt_match ais e p v1) s in
       (* We continue the application with the other arguments *)
       abstract_interpretation_application calls w1 vl'
    | SpecFun(n, cvl, fname, v0) ->
       (* Build the full list of parameters *)
       let full_vl = cvl @ vl in
       let s = List.length full_vl in

       (* If there are not enough parameters passed to the function, a new closure is returned
          For specified function, we proceed to evaluation once every arguments have been given *)
       if s < n then
         SM.return @@ Fun [SpecFun(n, full_vl, fname, v0)]

                          (* If all parameters were passed to the function, proceed to evaluation *)
       else
         (* We split the set of arguments in two: the exact number of arguments
            for the application and the remaining arguments *)
         let (vl, left_vl) = (List.filteri (fun i _ -> i < n) full_vl, List.filteri (fun i _ -> i >= n) full_vl) in
         let* new_vl = update_in fname vl in

         (* If the same call is found in the callstack, return Bot because we are looping *)
         if Calls.present calls fname new_vl then
           let* v = update_out fname new_vl Bot in
           SM.return v

                     (* If the call has never been done, we try to evaluate it *)
         else
           (* We push a new frame on the callstack and proceed to the evaluation *)
           let* v = abstract_interpretation_application (Calls.push (fname, new_vl) calls) v0 new_vl in
           let* v = update_out fname new_vl v in
           abstract_interpretation_application calls v left_vl
    | UnspecFun(n, cvl, f) ->
       (* For unspecified function, the application is also delayed until all
          arguments are passed to the function *)
       let full_vl = cvl @ vl in
       let s = List.length full_vl in

       (* If not enough arguments have been passed, return a new closure *)
       if s < n then
         SM.return @@ Fun [UnspecFun(n, full_vl, f)]
                          (* Otherwise, we proceed to evaluation *)
       else
         let (vl, left_vl) = (List.filteri (fun i _ -> i < n) full_vl, List.filteri (fun i _ -> i >= n) full_vl) in
         (* The function is fetched in the unspec_fun map *)
         let uf = SMap.find f unspec_funs in
         let* v = uf vl in
         abstract_interpretation_application calls v left_vl
end
