open Skeltypes
open Interv

module Help = Abstract.Help

module Test =
struct
  module SMap = Util.SMap

  type ppoint = Help.ppoint
  let virtual_pp_counter = ref 0
  let new_pp () =
    virtual_pp_counter := Int.add !virtual_pp_counter 1;
    !virtual_pp_counter

  (** Map from a program point to an association list that represents a state (maps variables to values) *)
  module StateMap =
    struct
      include Map.Make(struct type t = ppoint let compare = Help.compare_ppoint end)

      (** find_default x d m returns the current value of x in m. If no value is bound to m, returns d *)
      let find_default k d m =
        match find_opt k m with
        | None -> d
        | Some v -> v
    end

  (* The skeletal semantics *)
  let ss = Ss.ss_while

  module rec Types : sig
    type base_value =
      | Ident of string
      | Lit of int
      | State of ppoint * (state StateMap.t) * (state StateMap.t)* int
      | Int of Interv.t
      | Boolean of bool

    (* Association list for the states from identifiers to abstract values *)
    and state = (string * abst_value) list

    (* The abstract values, that are very similar to normal skeleton values *)
    and abst_value =
      | Tuple of abst_value list
      | Fun of abst_function list
      | Const of constructor * abst_value (* TODO: maybe use constructor type? Const of constructor * type * value *)
      | Base of base_value
      | PPoint of ppoint
      | Bot
      | Top

    (* The constructor for the different functions *)
    and abst_function =
      | SpecFun of int * abst_value list * string * abst_value
      | UnspecFun of int * abst_value list * string
      | Clos of pattern * env * skeleton

    (* The abstract environment *)
    and env =
      | BotEnv
      | Env of (string * abst_value) list
  end = Types

  include Types

  (* Printers *)
  let rec bv_to_string = function
    | Ident s -> s
    | Lit l -> Int.to_string l
    | State (pp, s, sout, _) -> (* TODO: do I want to print the set of calls? *)
      let s = StateMap.filter (fun pp _ -> not (Help.is_virtual pp)) s in
      let sout = StateMap.filter (fun pp _ -> not (Help.is_virtual pp)) sout in
      let pp_string = Help.ppoint_to_string pp in
      let states= StateMap.bindings s in
      let states_out= StateMap.bindings sout in

      let state_to_string (st: (string * abst_value) list) = (* Take a state (association list from string to abst_value) and returns a representation of this state in a string *)
        let bindslist = String.concat ", " (List.map (fun (ident, v) -> ident ^ " -> " ^ (av_to_string v)) st) in
        "[ " ^ bindslist ^ " ]" in

      "current ppoint: " ^ pp_string ^ "\n" ^
      String.concat "\n" (List.map (fun (pp, st) -> (Help.ppoint_to_string pp) ^ ": " ^ (state_to_string st)) states) ^
      "\nout\n" ^
      String.concat "\n" (List.map (fun (pp, st) -> (Help.ppoint_to_string pp) ^ ": " ^ (state_to_string st)) states_out)
    | Int i ->
      let low = Interv.inf i in
      let high = Interv.sup i in
      String.concat "" ["["; Float.to_string low; ", "; Float.to_string high; "]"]
    | Boolean b ->
      Bool.to_string b

  and av_to_string = function
    | Tuple vl ->
      let sl = List.map av_to_string vl in
      "(" ^ (String.concat ", " sl) ^ ")"
    | Fun _ -> "fun"
    | Const((_, cname, _), v) ->
    cname ^ "(" ^ (av_to_string v) ^ ")"
    | Base bv -> bv_to_string bv
    | PPoint pp ->
      Help.ppoint_to_string pp
    | Bot -> "⊥"
    | Top -> "⊤"

  let (pp_term: abst_value option ref) =  ref None
  let (_, stmt_type, _) = Util.SMap.find "stmt" ss.ss_types
  let pp_type_set = [stmt_type]

  let widen_threshold = 20

  let rec join_f (f: base_value -> base_value -> base_value) (v1: abst_value) (v2: abst_value): abst_value =
    match (v1, v2) with
    | (Tuple l1, Tuple l2) -> Tuple (List.map (fun (v1, v2) -> join_f f v1 v2) (List.combine l1 l2))
    | (Fun f1, Fun f2) -> Fun (f1 @ f2)
    | (Const(c1, v1), Const(c2, v2)) when c1 = c2 -> Const(c1, (join_f f v1 v2))
    | (Const(c1, _), Const(c2, _)) when c1 <> c2 -> Top
    | (Base bv1, Base bv2) -> Base (f bv1 bv2)
    | (Bot, v) | (v, Bot) -> v
    | (Top, _) | (_, Top) -> Top
    |  _ -> failwith "join: abstract values not joinable"

  let join_state (f: base_value -> base_value -> base_value) (st1: (string * abst_value) list) (st2: (string * abst_value) list) =
    let ids = List.sort_uniq Stdlib.compare (List.map fst (st1@st2)) in (* Retrieve the list of identifiers *)
    List.map
      (fun id ->
         match (List.assoc_opt id st1, List.assoc_opt id st2) with
         | (None, None) -> failwith "join_state: no values in either states, this case should not be possible"
         | (None, Some v) -> (id, v)
         | (Some v, None) -> (id, v)
         | (Some v1, Some v2) -> (id, join_f f v1 v2)
      ) ids

  let rec join_base_value (bv1: base_value) (bv2: base_value): base_value =
    match (bv1, bv2) with
    | (Int i1, Int i2) -> Int (Interv.hull i1 i2)
    | (State (pp1, s1, s1out, c1), State (pp2, s2, s2out, c2)) ->
      let s1l = StateMap.bindings s1 in
      let s2l = StateMap.bindings s2 in
      let s1outl = StateMap.bindings s1out in
      let s2outl = StateMap.bindings s2out in
      (* Retrieve the list of program points *)
      let pp_list = List.sort_uniq Stdlib.compare (List.map fst (s1l@s2l)) in
      let pp_out_list = List.sort_uniq Stdlib.compare (List.map fst (s1outl@s2outl)) in

      let fusion_assoc_l ppl s1l s2l =
        List.map (fun pp ->
            match (List.assoc_opt pp s1l, List.assoc_opt pp s2l) with
            | (None, None) -> failwith "join_state: no values in either states, this case should not be possible"
            | (None, Some v) -> (pp, v)
            | (Some v, None) -> (pp, v)
            | (Some v1, Some v2) -> (pp, join_state join_base_value v1 v2)
          ) ppl in

      let smapl = fusion_assoc_l pp_list s1l s2l in
      let smapoutl = fusion_assoc_l pp_out_list s1outl s2outl in

      let pp = Help.ppoint_common_prefix_or_virtual new_pp pp1 pp2 in
      let new_map = StateMap.of_seq (List.to_seq smapl) in
      let statepp1 = StateMap.find pp1 new_map in
      let statepp2 = StateMap.find pp2 new_map in
      let statepp = StateMap.find_default pp [] new_map in
      let new_map = StateMap.add pp (join_state join_base_value (join_state join_base_value statepp1 statepp2) statepp) new_map in

      let new_map_out = StateMap.of_seq (List.to_seq smapoutl) in
      let statepp1 = StateMap.find_default pp1 [] new_map_out in
      let statepp2 = StateMap.find_default pp2 [] new_map_out in
      let statepp = StateMap.find_default pp [] new_map_out in
      let new_map_out = StateMap.add pp (join_state join_base_value (join_state join_base_value statepp1 statepp2) statepp) new_map_out in

      State(pp,  new_map, new_map_out, Int.max c1 c2)
    | _ -> failwith "join_base_value: error"

  let rec widen (bv1: base_value) (bv2: base_value): base_value =
    match (bv1, bv2) with
    | (Int i1, Int i2) ->
      let inf = if Interv.inf i1 > Interv.inf i2 then Float.neg_infinity else Interv.inf i1 in
      let sup = if Interv.sup i1 < Interv.sup i2 then Float.infinity else Interv.sup i1 in
      Int (Interv.v inf sup)
    | (State (pp1, s1, s1out, c1), State (pp2, s2, s2out, c2)) ->
      let s1l = StateMap.bindings s1 in
      let s2l = StateMap.bindings s2 in
      let s1outl = StateMap.bindings s1out in
      let s2outl = StateMap.bindings s2out in
      (* Retrieve the list of program points *)
      let pp_list = List.sort_uniq Stdlib.compare (List.map fst (s1l@s2l)) in
      let pp_out_list = List.sort_uniq Stdlib.compare (List.map fst (s1outl@s2outl)) in

      let fusion_assoc_l ppl s1l s2l =
        List.map (fun pp ->
            match (List.assoc_opt pp s1l, List.assoc_opt pp s2l) with
            | (None, None) -> failwith "join_state: no values in either states, this case should not be possible"
            | (None, Some v) -> (pp, v)
            | (Some v, None) -> (pp, v)
            | (Some v1, Some v2) -> (pp, join_state widen v1 v2)
          ) ppl in

      let smapl = fusion_assoc_l pp_list s1l s2l in
      let smapoutl = fusion_assoc_l pp_out_list s1outl s2outl in

      let pp = Help.ppoint_common_prefix_or_virtual new_pp pp1 pp2 in
      let new_map = StateMap.of_seq (List.to_seq smapl) in
      let statepp1 = StateMap.find pp1 new_map in
      let statepp2 = StateMap.find pp2 new_map in
      let statepp = StateMap.find_default pp [] new_map in
      let new_map = StateMap.add pp (join_state widen (join_state widen statepp1 statepp2) statepp) new_map in

      let new_map_out = StateMap.of_seq (List.to_seq smapoutl) in
      let statepp1 = StateMap.find pp1 new_map_out in
      let statepp2 = StateMap.find pp2 new_map_out in
      let statepp = StateMap.find_default pp [] new_map_out in
      let new_map_out = StateMap.add pp (join_state widen (join_state widen statepp1 statepp2) statepp) new_map_out in

      State(pp,  new_map, new_map_out, Int.max c1 c2)
    | _ -> failwith "widen: error"

  (* The lesser functions *)
  let rec lesser (v1: abst_value) (v2: abst_value): bool =
    match (v1, v2) with
    | (Tuple l1, Tuple l2) ->
      List.for_all2 lesser l1 l2
    | (Fun _, Fun _) ->
      failwith "lesser: functions not comparable"
    | (Const(c1, v1), Const(c2, v2)) when c1 = c2 -> lesser v1 v2
    | (Const(c1, _), Const(c2, _)) when c1 <> c2 -> false
    | (PPoint pp1, PPoint pp2) -> pp1 = pp2
    | (Base bv1, Base bv2) -> lesser_base_value bv1 bv2
    | (Bot, _) -> true
    | (_, Bot) -> false
    | (_, Top) -> true
    | (Top, _) -> false
    |  _ -> false

  and lesser_state (st1: state) (st2: state): bool =
    List.for_all (fun (x, v) ->
        match List.assoc_opt x st2 with
        | None -> false
        | Some v' -> lesser v v'
      ) st1

  and lesser_base_value (bv1: base_value) (bv2: base_value): bool =
    match (bv1, bv2) with
    | (Int i1, Int i2) -> Interv.subset i1 i2
    | (State (_, s1, _, _), State (_, s2, _, _)) ->
      let s1_wo_virtual = StateMap.filter (fun pp _ -> not (Help.is_virtual pp)) s1 in
      StateMap.for_all (fun pp st1 ->
          match StateMap.find_opt pp s2 with
          | None -> false
          | Some st2 -> lesser_state st1 st2
        ) s1_wo_virtual
    | (Lit l1, Lit l2) -> l1 = l2
    | _ -> bv1 = bv2
      (*
    | _ -> failwith "lesser_base_value: uncompatible types"*)

  (* The mapping of with the unspecified functions and values *)
  let unspec_funs: (abst_value list -> abst_value) SMap.t = SMap.empty
  let unspec_vals: abst_value SMap.t = SMap.empty


  (* The updates functions *)
  let update_in s vl =
    if s = "eval_stmt" then
      match vl with
      | Base (State (pp, s, sout, c)) :: PPoint(ppt) :: [] ->
        let stpp = StateMap.find_default pp [] s in
        let stppt = StateMap.find_default ppt [] s in

        let new_state = join_state widen stppt stpp in
        let new_s = StateMap.add ppt new_state s in

        Base (State (ppt, new_s, sout, c)) :: PPoint(ppt) :: []
      | Base (State (_, s, sout, c)) :: Const(const, v) :: [] ->
        pp_term := Some (Const(const, v));
        Base(State (SubTerm([]), s, sout, c)) :: PPoint(SubTerm([])) :: []
      | _ ->
        vl
    else
      vl

  let update_out s vl x =
    if s = "eval_stmt" then
      match (x, vl) with
      | Base (State (pp, s, sout, c)), (Base (State (ppi, _, _, _))) :: _ :: [] ->
        let stpp = StateMap.find_default pp [] s in
        let stppiout = StateMap.find_default ppi [] sout in
        let st = join_state join_base_value stpp stppiout in
        let new_sout = StateMap.add ppi st sout in
        Base (State (pp, s, new_sout, c))
      | _ -> x
    else
      x
  let int_constr:constructor = Abstract.Help.get_constr ss "Int" "value"

  let litToVal (vl: abst_value list): abst_value =
    match vl with
    | Base (Lit l) :: [] ->
      Const(int_constr, Base (Int (Interv.of_int l)))
    | _ -> failwith "litToVal: wrong number of arguments or wrong types"

  let read (vl: abst_value list): abst_value =
    match vl with
    | Base (Ident x) :: Base (State (pp, s, _, _)) :: [] ->
      let st = StateMap.find pp s in
      List.assoc x st
    | _ -> failwith "read: wrong number of arguments or wrong types"

  let write (vl: abst_value list): abst_value =
    match vl with
    | Base (Ident x) :: Base (State (pp, s, sout, c)) :: v :: [] ->
      let stpp = StateMap.find pp s in
      let new_stpp = (x, v) :: (List.remove_assoc x stpp) in
    let virtual_pp = Help.Virtual(new_pp ()) in
    Base (State (virtual_pp, StateMap.add virtual_pp new_stpp s, sout, c))
    | _ -> failwith "write: wrong number of arguments or wrong types"

  let add(vl: abst_value list): abst_value =
    match vl with
    | Base (Int n1) :: Base (Int n2) :: [] ->
        Base (Int (n1 +! n2))
    | _ -> failwith "add: wrong number of arguments or wrong types"

  let eq(vl: abst_value list): abst_value =
    match vl with
    | Base (Int n1) :: Base (Int n2) :: [] ->
      if (Interv.is_singleton n1) && (n1 = n2) then Base (Boolean(true))
      else if (Interv.disjoint n1 n2) then Base (Boolean(false))
      else Top
    | _ -> failwith "eq: wrong number of arguments or wrong types"

  let lt(vl: abst_value list): abst_value =
    match vl with
    | Base (Int n1) :: Base (Int n2) :: [] ->
      if (Interv.compare_f n1 (Interv.inf n2)) = 1 then Base (Boolean(true))
      else if (Interv.compare_f (n1 + Interv.one) (Interv.sup n2)) = -1 then Base (Boolean(false))
      else Top
    | _ -> failwith "lt: wrong number of arguments or wrong types"

  let rand (vl: abst_value list): abst_value =
    match vl with
    | Base (Lit l1) :: Base (Lit l2) :: [] ->
      Base (Int (Interv.interv l1 l2))
    | _ -> failwith "rand: wrong number of arguments or wrong types"

  let isTrue (vl: abst_value list): abst_value =
    match vl with
    | Base (Boolean(true)) :: [] -> Tuple([])
    | Top :: [] -> Tuple([])
    | Base (Boolean(false)) :: [] -> Bot
    | _ -> failwith "is_true: wrong number of arguments or wrong types"

  let isFalse(vl: abst_value list): abst_value =
    match vl with
    | Base (Boolean(false)) :: [] -> Tuple([])
    | Top :: [] -> Tuple([])
    | Base (Boolean(true)) :: [] -> Bot
    | _ -> failwith "is_true: wrong number of arguments or wrong types"


  let unspec_funs = SMap.add "litToVal" litToVal unspec_funs
  let unspec_funs = SMap.add "read" read unspec_funs
  let unspec_funs = SMap.add "write" write unspec_funs
  let unspec_funs = SMap.add "add" add unspec_funs
  let unspec_funs = SMap.add "eq" eq unspec_funs
  let unspec_funs = SMap.add "lt" lt unspec_funs
  let unspec_funs = SMap.add "rand" rand unspec_funs
  let unspec_funs = SMap.add "isTrue" isTrue unspec_funs
  let unspec_funs = SMap.add "isFalse" isFalse unspec_funs

  let unspec_vals = SMap.add "litToVal"  (Fun([UnspecFun(1, [], "litToVal")])) unspec_vals
  let unspec_vals = SMap.add "read"  (Fun([UnspecFun(2, [], "read")])) unspec_vals
  let unspec_vals = SMap.add "write"  (Fun([UnspecFun(3, [], "write")])) unspec_vals
  let unspec_vals = SMap.add "add"  (Fun([UnspecFun(2, [], "add")])) unspec_vals
  let unspec_vals = SMap.add "eq"  (Fun([UnspecFun(2, [], "eq")])) unspec_vals
  let unspec_vals = SMap.add "lt"  (Fun([UnspecFun(2, [], "lt")])) unspec_vals
  let unspec_vals = SMap.add "rand"  (Fun([UnspecFun(2, [], "rand")])) unspec_vals
  let unspec_vals = SMap.add "isTrue"  (Fun([UnspecFun(1, [], "isTrue")])) unspec_vals
  let unspec_vals = SMap.add "isFalse"  (Fun([UnspecFun(1, [], "isFalse")])) unspec_vals
  let unspec_vals = SMap.add "empty_state" (Base (State(SubTerm([]), StateMap.add (SubTerm []) [] StateMap.empty, StateMap.empty, 0))) unspec_vals
  let unspec_vals = SMap.add "x" (Base(Ident ("x"))) unspec_vals
  let unspec_vals = SMap.add "y" (Base(Ident ("y"))) unspec_vals
  let unspec_vals = SMap.add "z" (Base(Ident ("z"))) unspec_vals
  let unspec_vals = SMap.add "zero" (Base(Lit 0)) unspec_vals
  let unspec_vals = SMap.add "mone" (Base(Lit (-1))) unspec_vals
  let unspec_vals = SMap.add "one" (Base(Lit 1)) unspec_vals
  let unspec_vals = SMap.add "two" (Base(Lit 2)) unspec_vals
  let unspec_vals = SMap.add "three" (Base(Lit 3)) unspec_vals
end
