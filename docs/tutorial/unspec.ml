open Skeltypes

(** The Unspec module with everything needed to defined the unspecified terms and types of a skeletal semantics *)

(** The unspecified module signature which gives the types and functions that must be given to perform an abstract interpretation *)
module type UNSPEC =
sig
  module SMap = Util.SMap

  (* The type of basic values *)
  type base_value


  (* The type of program-points *)
  type ppoint = Help.ppoint

  (* The abstract values, that are very similar to normal skeleton values *)
  type abst_value =
    | Tuple of abst_value list
    | Fun of abst_function list
    | Const of constructor * abst_value (* TODO: maybe use constructor type? Const of constructor * type * value *)
    | Base of base_value
    | PPoint of ppoint
    | Bot
    | Top

  (* The constructor for the different functions *)
  and abst_function =
    | SpecFun of int * abst_value list * string * abst_value
    | UnspecFun of int * abst_value list * string
    | Clos of pattern * env * skeleton

  (* The abstract environment *)
  and env =
    | BotEnv
    | Env of (string * abst_value) list

  (* Printer for base values and abstract values *)
  val bv_to_string: base_value -> string
  val av_to_string: abst_value -> string

  (* The join functions for base and abstract values, and a widening operator on base values*)
  val join_f: (base_value -> base_value -> base_value) -> abst_value -> abst_value -> abst_value
  val join_base_value: base_value -> base_value -> base_value
  val widen: base_value -> base_value -> base_value

  (* Functions to test if an abstract value is included in another abstract value *)
  val lesser: abst_value -> abst_value -> bool
  val lesser_base_value: base_value -> base_value -> bool

  (* The mapping of with the unspecified functions and values *)
  val unspec_funs: (abst_value list -> abst_value) SMap.t
  val unspec_vals: abst_value SMap.t

  (* The set of types that can be replaced with program-points, the term replace by program points *)
  val pp_type_set: necro_type list
  val pp_term: abst_value option ref

  (* The skeletal semantics *)
  val ss: Skeltypes.skeletal_semantics

  (* Updates functions *)
  val update_in: string -> abst_value list -> abst_value list
  val update_out: string -> abst_value list -> abst_value -> abst_value
end
