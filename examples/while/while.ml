open Skeltypes
open Interv
open Abstract

module SM = Statemonad
let ( let* ) = SM.binding

module PPMap = struct
  include Map.Make(struct type t = Ppoint.t let compare = Stdlib.compare end)

  (* find with default value *)
  let find_default (k: key) (def: 'a) (map: 'a t): 'a =
    if mem k map then
      find k map
    else
      def
end

type ppoint = Ppoint.t
(** Definition of abstract interpretation for while with abstract domain *)

module Unspec = struct

  type 'abst ai_state = { stores: ((string * 'abst) list) PPMap.t; prg: 'abst }
  let init_ai_state (prg: 'abst) =
    { stores = PPMap.add (Ppoint.epsilon) [] PPMap.empty; prg = prg }
  let join_ai_state _ _ = failwith "join_ai_state: unimplemented"

  let get_prg (ais: 'abst ai_state) = ais.prg

  let string_of_ai_state (ais: 'abst ai_state) (pra: 'abst -> string): string =
    PPMap.fold
      (fun pp v s ->
         (Ppoint.to_string pp) ^ ": [" ^ (String.concat ", " (List.map (fun (n, v) -> n ^ ": " ^ (pra v)) v)) ^ "]\n" ^ s
      ) ais.stores ""

  type 'abst base_value =
    | Ident of string
    | Lit of int
    | Store of (string * 'abst) list
    | Int of Interv.t
    | Boolean of bool

  (* Printers *)
  let bv_to_string (_: 'abst ai_state) (pra: 'abst -> string) = function
    | Ident s -> s
    | Lit l -> Int.to_string l
    | Store st ->
      "[" ^ (String.concat ", " (List.map (fun (n, v) -> n ^ ": " ^ (pra v)) st)) ^ "]"
    | Int i ->
      let low = Float.to_string (Interv.inf i) in
      let high = Float.to_string (Interv.sup i) in
      String.concat "" ["["; low; ", "; high; "]"]
    | Boolean b ->
      Bool.to_string b

  (* Comparison functions*)
  let rec lesser_base_value (_: 'abst ai_state) (lesser: 'abst -> 'abst -> bool) (bv1: 'abst base_value) (bv2: 'abst base_value): bool =
    match (bv1, bv2) with
    | (Int i1, Int i2) -> Interv.subset i1 i2
    | (Store s1, Store s2) ->
      lesser_store lesser s1 s2
    | (Lit l1, Lit l2) -> l1 = l2
    | _ -> bv1 = bv2

  and lesser_store (lesser: 'abst -> 'abst -> bool) (st1: (string * 'abst) list) (st2: (string * 'abst) list): bool =
    List.for_all (fun (x, v) ->
        match List.assoc_opt x st2 with
        | None -> false
        | Some v' -> lesser v v'
      ) st1

  (* Union functions *)
  let join_store (join: 'abst -> 'abst -> ('abst ai_state, 'abst) SM.t) st1 st2 =
    let ids_of_st1 = List.map fst st1 in
    let ids_of_st2 = List.map fst st2 in
    let ids_of_st1_st2 = Help.remove_duplicates String.equal (ids_of_st1 @ ids_of_st2) in
    SM.foldm_list
      (fun st id ->
        match (List.assoc_opt id st1, List.assoc_opt id st2) with
        | (Some v1, Some v2) ->
           let* v = join v1 v2 in
           SM.return @@ (id, v) :: st
        | (Some v, None) | (None, Some v) ->
           SM.return @@ (id, v) :: st
        | (None, None) ->
           SM.return st
      )
      [] ids_of_st1_st2

  let join_base_value (join: 'abst -> 'abst -> ('abst ai_state, 'abst) SM.t) (bv1: 'abst base_value) (bv2: 'abst base_value): ('abst ai_state, 'abst base_value) SM.t =
    match (bv1, bv2) with
    | (Int i1, Int i2) ->
       SM.return @@ Int (Interv.hull i1 i2)
    | (Store st1, Store st2) ->
      (* New store *)
       let* st = join_store join st1 st2 in
       SM.return @@ Store st
    | _ -> failwith "join_base_value: expected int or store"

  let widen_base_value (widen: 'abst -> 'abst -> ('abst ai_state, 'abst) SM.t) (bv1: 'abst base_value) (bv2: 'abst base_value): ('abst ai_state, 'abst base_value) SM.t =
    match (bv1, bv2) with
    | (Int i1, Int i2) ->
       let inf = if Interv.inf i1 > Interv.inf i2 then Float.neg_infinity else Interv.inf i1 in
       let sup = if Interv.sup i1 < Interv.sup i2 then Float.infinity else Interv.sup i1 in
       SM.return @@ Int (Interv.v inf sup)
    | (Store st1, Store st2) ->
       (* New store *)
       let* st = join_store widen st1 st2 in
       SM.return @@ Store st
    | _ -> failwith "join_base_value: error"

  let ss = Ss.ss_while
end


open Abstract.Types
module WhileTypes = BuildTypes(Unspec)

module While = struct
  include WhileTypes

  let join_store = Unspec.join_store

  let (_, stmt_type, _) = Util.SMap.find "stmt" ss.ss_types
  let pp_type_set = [stmt_type]

  let int_constr:constructor = Abstract.Help.get_constr ss "Int" "value"

  (* The updates functions *)
  let update_in (fun_name: string) (vl: abst_value list): (ai_state, abst_value list) SM.t =
    if fun_name = "eval_stmt" then
      match vl with
      | Base (Store (st)) :: PPoint(ppt) :: [] ->
         let* (ais: ai_state) = SM.get () in

         let st_ppt = PPMap.find_default ppt [] ais.stores in
         let* new_st = join_store (join_f widen_base_value) st_ppt st in

         let new_ais = {ais with stores = (PPMap.add ppt new_st ais.stores); } in

         let* () = SM.put new_ais in

         SM.return @@ Base (Store (new_st)) :: PPoint (ppt) :: []
      | Base (s) :: Const(_, _) :: [] ->
         SM.return @@ Base(s) :: PPoint(SubTerm(In, [])) :: []
      | _ -> failwith "update_in: invalid values"
    else
      SM.return vl

  let update_out (fun_name: string) (vl: abst_value list) (v: abst_value): (ai_state, abst_value) SM.t =
    if fun_name = "eval_stmt" then
      match (v, vl) with
      | Base (Store (st)), _ :: PPoint SubTerm(_, ppil) :: [] ->
         let* (ais: ai_state) = SM.get () in

         let ppi = Ppoint.SubTerm(Out, ppil) in
         let stppiout = PPMap.find_default ppi [] ais.stores in
         let* st = join_store (join_f join_base_value) st stppiout in

         let new_ais = {ais with stores = PPMap.add ppi st ais.stores } in

         let* () = SM.put new_ais in

         SM.return @@ Base (Store (st))
      | Bot, _ :: PPoint SubTerm(_, ppil) :: [] ->
         let* (ais: ai_state) = SM.get () in
         SM.return @@ Base (Store (PPMap.find_default (SubTerm(Out, ppil)) [] ais.stores))
      | _ -> failwith "update_out: invalid values"
    else
      SM.return v

  (* Definition of the unspecified function *)
  let litToVal (vl: abst_value list): (ai_state, abst_value) SM.t =
    match vl with
    | Base (Lit l) :: [] ->
      SM.return @@ Const(int_constr, Base (Int (Interv.of_int l)))
    | _ -> failwith "litToVal: wrong number of arguments or wrong types"

  let read (vl: abst_value list): (ai_state, abst_value) SM.t =
    match vl with
    | Base (Ident x) :: Base (Store st) :: [] ->
      SM.return @@ List.assoc x st
    | _ -> failwith "read: wrong number of arguments or wrong types"

  let write (vl: abst_value list): (ai_state, abst_value) SM.t =
    match vl with
    | Base (Ident x) :: Base (Store st) :: v :: [] ->
       let new_st = (x, v) :: List.remove_assoc x st in
       SM.return @@ Base (Store new_st)
    | _ -> failwith "write: wrong number of arguments or wrong types"

  let add (vl: abst_value list): (ai_state, abst_value) SM.t =
    match vl with
    | Base (Int n1) :: Base (Int n2) :: [] ->
        SM.return @@ Base (Int (n1 +! n2))
    | _ -> failwith "add: wrong number of arguments or wrong types"

  let inc (vl: abst_value list): (ai_state, abst_value) SM.t =
    match vl with
    | Base (Int n) :: [] ->
        SM.return @@ Base (Int (n +! Interv.one))
    | _ -> failwith "inc: wrong number of arguments or wrong types"

  let eq (vl: abst_value list): (ai_state, abst_value) SM.t =
    match vl with
    | Base (Int n1) :: Base (Int n2) :: [] ->
       if (Interv.is_singleton n1) && (n1 = n2) then
         SM.return @@ Base (Boolean(true))
       else if (Interv.disjoint n1 n2) then
         SM.return @@ Base (Boolean(false))
       else 
         SM.return Top
    | _ -> failwith "eq: wrong number of arguments or wrong types"

  let lt (vl: abst_value list): (ai_state, abst_value) SM.t =
    match vl with
    | Base (Int n1) :: Base (Int n2) :: [] ->
       if (Interv.compare_f n1 (Interv.inf n2)) = 1 then
         SM.return @@ Base (Boolean(true))
       else if (Interv.compare_f (n1 + Interv.one) (Interv.sup n2)) = -1 then
         SM.return @@ Base (Boolean(false))
       else
         SM.return Top
    | _ -> failwith "lt: wrong number of arguments or wrong types"

  let le (vl: abst_value list): (ai_state, abst_value) SM.t =
    match vl with
    | Base (Int n1) :: Base (Int n2) :: [] ->
       if (Interv.compare_f n1 Stdlib.(Interv.inf n2 -. 1.)) = 1 then
         SM.return @@ Base (Boolean(true))
       else if (Interv.compare_f n1 (Interv.sup n2)) = -1 then
         SM.return @@ Base (Boolean(false))
       else
         SM.return Top
    | _ -> failwith "le: wrong number of arguments or wrong types"

  let rand (vl: abst_value list): (ai_state, abst_value) SM.t =
    match vl with
    | Base (Lit l1) :: Base (Lit l2) :: [] ->
      SM.return @@ Base (Int (Interv.interv l1 l2))
    | _ -> failwith "rand: wrong number of arguments or wrong types"

  let isTrue (vl: abst_value list): (ai_state, abst_value) SM.t =
    match vl with
    | Base (Boolean(true)) :: []
    | Top :: [] ->
       SM.return @@ Tuple([[]])
    | Base (Boolean(false)) :: [] ->
       SM.return Bot
    | _ -> failwith "isTrue: wrong number of arguments or wrong types"

  let isFalse (vl: abst_value list): (ai_state, abst_value) SM.t =
    match vl with
    | Base (Boolean(false)) :: []
    | Top :: [] ->
       SM.return @@ Tuple([[]])
    | Base (Boolean(true)) :: [] ->
       SM.return Bot
    | _ -> failwith "isFalse: wrong number of arguments or wrong types"


  let unspec_funs = SMap.empty
  let unspec_funs = SMap.add "litToVal" litToVal unspec_funs
  let unspec_funs = SMap.add "read" read unspec_funs
  let unspec_funs = SMap.add "write" write unspec_funs
  let unspec_funs = SMap.add "add" add unspec_funs
  let unspec_funs = SMap.add "inc" inc unspec_funs
  let unspec_funs = SMap.add "eq" eq unspec_funs
  let unspec_funs = SMap.add "lt" lt unspec_funs
  let unspec_funs = SMap.add "le" le unspec_funs
  let unspec_funs = SMap.add "rand" rand unspec_funs
  let unspec_funs = SMap.add "isTrue" isTrue unspec_funs
  let unspec_funs = SMap.add "isFalse" isFalse unspec_funs

  let unspec_vals = SMap.add "litToVal"  (Fun([UnspecFun(1, [], "litToVal")])) unspec_vals
  let unspec_vals = SMap.add "read"  (Fun([UnspecFun(2, [], "read")])) unspec_vals
  let unspec_vals = SMap.add "write"  (Fun([UnspecFun(3, [], "write")])) unspec_vals
  let unspec_vals = SMap.add "add"  (Fun([UnspecFun(2, [], "add")])) unspec_vals
  let unspec_vals = SMap.add "inc"  (Fun([UnspecFun(1, [], "inc")])) unspec_vals
  let unspec_vals = SMap.add "eq"  (Fun([UnspecFun(2, [], "eq")])) unspec_vals
  let unspec_vals = SMap.add "lt"  (Fun([UnspecFun(2, [], "lt")])) unspec_vals
  let unspec_vals = SMap.add "le"  (Fun([UnspecFun(2, [], "le")])) unspec_vals
  let unspec_vals = SMap.add "rand"  (Fun([UnspecFun(2, [], "rand")])) unspec_vals
  let unspec_vals = SMap.add "isTrue"  (Fun([UnspecFun(1, [], "isTrue")])) unspec_vals
  let unspec_vals = SMap.add "isFalse"  (Fun([UnspecFun(1, [], "isFalse")])) unspec_vals
  let unspec_vals = SMap.add "empty_store" (Base (Store ([]))) unspec_vals
  let unspec_vals = SMap.add "x" (Base(Ident ("x"))) unspec_vals
  let unspec_vals = SMap.add "y" (Base(Ident ("y"))) unspec_vals
  let unspec_vals = SMap.add "z" (Base(Ident ("z"))) unspec_vals
  let unspec_vals = SMap.add "zero" (Base(Lit 0)) unspec_vals
  let unspec_vals = SMap.add "mone" (Base(Lit (-1))) unspec_vals
  let unspec_vals = SMap.add "one" (Base(Lit 1)) unspec_vals
  let unspec_vals = SMap.add "two" (Base(Lit 2)) unspec_vals
  let unspec_vals = SMap.add "three" (Base(Lit 3)) unspec_vals
end
