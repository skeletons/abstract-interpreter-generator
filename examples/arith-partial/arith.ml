open Skeltypes
open Interv

module Help = Abstract.Help
module Ppoint = Abstract.Ppoint

module Test =
struct
  module SMap = Util.SMap

  type base_value =
    | Nat of Interv.t
    | Lit of int

  type ppoint = Ppoint.t

  (* The abstract values, that are very similar to normal skeleton values *)
  type abst_value =
    | Tuple of abst_value list
    | Fun of abst_function list
    | Const of constructor * abst_value
    | Base of base_value
    | PPoint of ppoint
    | Bot
    | Top

  (* The constructor for the different functions *)
  and abst_function =
    | SpecFun of int * abst_value list * string * abst_value
    | UnspecFun of int * abst_value list * string
    | Clos of pattern * env * skeleton

  (* The abstract environment *)
  and env =
    | BotEnv
    | Env of (string * abst_value) list

  (* Printers *)
  let bv_to_string = function
    | Nat i ->
      let low = Float.to_int (Interv.inf i) in
      let high = Float.to_int (Interv.sup i) in
      String.concat "" ["["; Int.to_string low; ", "; Int.to_string high; "]"]
    | Lit l -> Int.to_string l

  let rec av_to_string = function
    | Tuple vl ->
      let sl = List.map av_to_string vl in
      "(" ^ (String.concat ", " sl) ^ ")"
    | Fun fl ->
      let fun_to_string (f: abst_function): string =
        match f with
        | Clos(_) -> "closure"
        | UnspecFun(_, vl, f) ->
          let args_string = String.concat " " (List.map av_to_string vl) in
          f ^ " " ^ args_string
        | SpecFun(_, vl, f, _) ->
          let args_string = String.concat " " (List.map av_to_string vl) in
          f ^ " " ^ args_string in
      String.concat ", " (List.map fun_to_string fl)
    | Const((_, cname, _), v) ->
      cname ^ "(" ^ (av_to_string v) ^ ")"
    | Base bv -> bv_to_string bv
    | PPoint pp ->
      Ppoint.to_string pp
    | Bot -> "⊥"
    | Top -> "⊤"


  (* set of ppoint types *)
  let pp_type_set = []
  let (pp_term: abst_value option ref) = ref None

  (* update functions *)
  let update_in _ x = x
  let update_out _ _  x = x

  let rec join_f (f: base_value -> base_value -> base_value) (v1: abst_value) (v2: abst_value): abst_value =
    match (v1, v2) with
    | (Tuple l1, Tuple l2) -> Tuple (List.map (fun (v1, v2) -> join_f f v1 v2) (List.combine l1 l2))
    | (Fun f1, Fun f2) -> Fun (f1 @ f2)
    | (Const(c1, v1), Const(c2, v2)) when c1 = c2 -> Const(c1, (join_f f v1 v2))
    | (Const(c1, _), Const(c2, _)) when c1 <> c2 -> Top
    | (Base bv1, Base bv2) -> Base (f bv1 bv2)
    | (Bot, v) | (v, Bot) -> v
    | (Top, _) | (_, Top) -> Top
    |  _ -> failwith "join: abstract values not joinable"

  let join_base_value (bv1: base_value) (bv2: base_value): base_value =
    match (bv1, bv2) with
    | (Nat i1, Nat i2) -> Nat (Interv.hull i1 i2)
    | _ -> failwith "join_base_value: error"

  let widen = join_base_value

  (* The lesser functions *)
  let rec lesser (v1: abst_value) (v2: abst_value): bool =
    match (v1, v2) with
    | (Tuple l1, Tuple l2) ->
      List.for_all2 lesser l1 l2
    | (Fun _, Fun _) ->
      failwith "lesser: functions not comparable"
    | (Const(c1, v1), Const(c2, v2)) when c1 = c2 -> lesser v1 v2
    | (Const(c1, _), Const(c2, _)) when c1 <> c2 -> false
    | (PPoint pp1, PPoint pp2) -> pp1 = pp2
    | (Base bv1, Base bv2) -> lesser_base_value bv1 bv2
    | (Bot, _) -> true
    | (_, Bot) -> false
    | (_, Top) -> true
    | (Top, _) -> false
    |  _ -> false

  and lesser_base_value (bv1: base_value) (bv2: base_value): bool =
    match (bv1, bv2) with
    | (Nat i1, Nat i2) -> Interv.subset i1 i2
    | (Lit l1, Lit l2) -> l1 = l2
    | _ -> false

  (* The mapping of with the unspecified functions and values *)
  let unspec_funs: (abst_value list -> abst_value) SMap.t = SMap.empty
  let unspec_vals: abst_value SMap.t = SMap.empty

  let plus (vl: abst_value list): abst_value =
    match vl with
    | Base (Nat n1) :: Base (Nat n2) :: Base (Nat n3) :: [] ->
    Base (Nat (n1 +! n2 +! n3))
    | _ -> failwith "plus: wrong number of arguments or wrong types"

  let litToNat (vl: abst_value list): abst_value =
    match vl with
    | Base (Lit n) :: [] -> Base (Nat (Interv.of_int n))
    | _ -> failwith "plus: wrong number of arguments or wrong types"

  let unspec_funs = SMap.add "plus" plus unspec_funs
  let unspec_funs = SMap.add "litToNat" litToNat unspec_funs

  let unspec_vals = SMap.add "plus" (Fun([UnspecFun(3, [], "plus")])) unspec_vals
  let unspec_vals = SMap.add "litToNat" (Fun([UnspecFun(1, [], "litToNat")])) unspec_vals
  let unspec_vals = SMap.add "one" (Base(Lit 1)) unspec_vals
  let unspec_vals = SMap.add "mone" (Base(Lit (-1))) unspec_vals
  let unspec_vals = SMap.add "two" (Base(Lit 2)) unspec_vals
  let unspec_vals = SMap.add "three" (Base(Lit 3)) unspec_vals

  let ss = Ss.ss_arith_partial
end
