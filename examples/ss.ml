open Necro

let path = List.hd Skelsites.Sites.skelsems

let ss_arith = Skeleton.lss_to_ss (Necro.parse_and_type [] (path ^ "/arith.sk"))
let ss_arith_partial = Skeleton.lss_to_ss (Necro.parse_and_type [] (path ^ "/arith-partial.sk"))
let ss_arithmetic = Skeleton.lss_to_ss (Necro.parse_and_type [] (path ^ "/arithmetic.sk"))
let ss_while = Skeleton.lss_to_ss (Necro.parse_and_type [] (path ^ "/while.sk"))
let ss_lambda = Skeleton.lss_to_ss (Necro.parse_and_type [] (path ^ "/lambda.sk"))
