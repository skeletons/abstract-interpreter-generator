module Help = Abstract.Help
module PPoint = Abstract.Ppoint
type ppoint = Abstract.Ppoint.t

module SM = Abstract.Statemonad
let ( let* ) = SM.binding

(** A map module where keys are program points **)
module PPMap = struct
  include Map.Make(struct type t = ppoint let compare = Stdlib.compare end)

  (* find with default value *)
  let find_default (k: key) (def: 'a) (map: 'a t): 'a =
    if mem k map then
      find k map
    else
      def
end

(** The module where unspecified values and types for the lambda-calculus are defined *)
module LambdaUnspec =
  struct
    type var = string

    (* Module defining abstract closure *)
    module AClos = struct
      type t = ppoint
      let compare = PPoint.compare

      let to_string (pp: t): string =
        Printf.sprintf "(%s)" (PPoint.to_string pp)
    end

    (* Module defining set of abstract closures *)
    module AClosSetBase = Set.Make (AClos)

    (* Same module but with printing function *)
    module AClosSet = struct
      include AClosSetBase

      let to_string (cs: AClosSetBase.t): string =
        let s = String.concat ", " (List.map AClos.to_string (AClosSetBase.elements cs)) in
        "{" ^ s ^ "}"
    end

    (* The abstraction of state of lambda-calculus, represented as an association list *)
    module State = struct
      type t = (var * AClosSet.t) list

      let to_string (s: t) =
        (String.concat ", "
           (List.map
              (fun (n, cs) ->
                n ^ " -> (" ^ (AClosSet.to_string cs) ^ ")")
              s)
        )

    end

    type state = State.t

    (* Comparison of states *)
    let lesser_state s1 s2 =
      let keys = List.map fst s1 in
      List.for_all (fun n ->
          match (List.assoc_opt n s1, List.assoc_opt n s2) with
          | (None, _) -> true
          | (Some _, None) -> false
          | (Some cs1, Some cs2) -> AClosSet.subset cs1 cs2
        ) keys

    (* The state of the abstract interpretation: a map from ppoint to abstract environment, and a map from ppoint to a set of abstract closures
       Moreover there is the current program being evaluated *)
    type 'abst ai_state = { rho: state PPMap.t ; c: AClosSet.t PPMap.t; prg : 'abst }

    (* Function initializing the ai state with the current program *)
    let init_ai_state (prg: 'abst): 'abst ai_state =
      let rho = PPMap.add PPoint.epsilon [] PPMap.empty in
      let c = PPMap.add PPoint.epsilon AClosSet.empty PPMap.empty in
      { rho; c; prg}

    (* Printing function for ai state *)
    let string_of_ai_state (ais: 'abst ai_state) (_: 'abst -> string): string =
      let rho_string =
        PPMap.fold
          (fun pp v str ->
            str ^
              (PPoint.to_string pp) ^ " : [" ^ (String.concat ", "
                                                  (List.map
                                                     (fun (n, cs) ->
                                                       n ^ " -> (" ^ (AClosSet.to_string cs) ^ ")")
                                                     v)
                                               ) ^ "]\n"
          ) ais.rho "" in
      let c_string =
        (* (String.concat "\n" (List.map (fun (n, aclos) -> (PPoint.to_string n) ^ " : " ^ (AClosSet.to_string aclos)) (PPMap.bindings ais.c))) in *)
        PPMap.fold
          (fun pp aclos str ->
            str ^ (PPoint.to_string pp) ^ " : " ^ (AClosSet.to_string aclos) ^ "\n"
          ) ais.c "" in
      Printf.sprintf "rho:\n%sc:\n%s" rho_string c_string

    (* The type of base values *)
    type 'abst base_value =
      | Ident of var
      | Env of state
      | AClos of AClosSet.t

    (* Printing functions for base values *)
    let bv_to_string (_: 'abst ai_state) (_: 'abst -> string) = function
      | Ident v -> v
      | Env st ->
         State.to_string st
      | AClos cl ->
         AClosSet.to_string cl


    (* Comparison functions for base values *)
    let lesser_base_value (_: 'abst ai_state)(_: 'abst -> 'abst -> bool) (bv1: 'abst base_value) (bv2: 'abst base_value): bool =
      match (bv1, bv2) with
      | (Ident v1, Ident v2) -> v1 = v2
      | (Env st1, Env st2) ->
         lesser_state st1 st2
      | (AClos cl1, AClos cl2) ->
         AClosSet.subset cl1 cl2
      | (_, _) ->
         failwith "lesser_base_value: type error"

    (* Equality for ai_state *)
    let equal_ai_state ais1 ais2=
      let cmp s1 s2 =
        let keys = List.map fst s1 in
        List.for_all (fun n ->
            match (List.assoc_opt n s1, List.assoc_opt n s2) with
            | (Some cs1, Some cs2) -> AClosSet.equal cs1 cs2
            | _ -> false
          ) keys in
      PPMap.equal cmp ais1.rho ais2.rho && PPMap.equal AClosSet.equal ais1.c ais2.c

    (* Union functions for states *)
    let join_state (s1: state) (s2: state): state =
      let assoc k s =
        match List.assoc_opt k s with
        | Some v -> v
        | None -> AClosSet.empty in

      let keys = Abstract.Help.remove_duplicates (=) (List.map fst (s1 @ s2)) in
      List.map (fun k -> k, AClosSet.union (assoc k s1) (assoc k s2)) keys

    (* Union for base values *)
    let join_base_value _ (bv1: 'abst base_value) (bv2: 'abst base_value): ('abst ai_state, 'abst base_value) SM.t =
      match (bv1, bv2) with
      | (Env st1, Env st2) ->
         let st = join_state st1 st2 in
         SM.return @@ Env (st)
      | (AClos cl1, AClos cl2) ->
         SM.return @@ AClos (AClosSet.union cl1 cl2)
      | _ -> failwith "join_base_value: error"

    (* Widening for base values *)
    let widen_base_value = join_base_value

    (* Function that returns the current program *)
    let get_prg (ais: 'abst ai_state) = ais.prg

    (* The skeletal semantics of lambda-calculus *)
    let ss = Ss.ss_lambda
  end

open Abstract.Types
(* BuildTypes returns a module with the abst_value type, union and comparison functions *)
module LambdaTypes = BuildTypes(LambdaUnspec)

(* Mainly, this module extends LambdaTypes with the definition of the unspecified functions *)
module Lambda = struct
  module AClosSet = LambdaUnspec.AClosSet
  include LambdaTypes
  type state = LambdaUnspec.state

  let (_, lterm_type, _) = Util.SMap.find "lterm" ss.ss_types

  (* This set contains types to be replaced with program points when unfolding. Here, the set only contains lterm type *)
  let pp_type_set = [ lterm_type ]

  (* Update functions *)
  let update_in s vl: (ai_state, abst_value list) SM.t =
    (* [eval ppₛ ppₜ] is changed in [eval ppₜ ppₜ], and the ai state map ρ is changed such that
       ρ[ppₜ → ρ(ppₜ) ⊔ ρ(ppₛ)] *)
    if s = "eval" then
      match vl with
      | Base (Env(st)) :: PPoint(ppt) :: [] ->
         let* (ais: ai_state) = SM.get () in

         (* Retrieving ρ(ppₜ) and ρ(ppₛ)*)
         let st_ppt = PPMap.find_default ppt [] ais.rho in

         (* Computes ρ(ppₜ) ⊔ ρ(ppₛ) and stores it in ρ(ppₜ) *)
         let new_st = LambdaUnspec.join_state st_ppt st in
         let rho = PPMap.add ppt new_st ais.rho in

         let* () = SM.put { ais with rho } in

         SM.return @@ Base (Env(new_st)) :: PPoint (ppt) :: []
      | Base (s) :: Const(_, _) :: [] ->
         SM.return @@ Base(s) :: PPoint(SubTerm(In, [])) :: []
      | _ -> failwith "update_in: args do not match"
    else
      SM.return @@ vl

  let update_out s vl v =
    (* The result of [eval ppₜ ppₜ] is added to C(ppₜ) *)
    if s = "eval" then
      match (vl, v) with
      | (_, Bot) ->
         SM.return v
      | (_ :: PPoint (ppt) :: [], Base (AClos aclos)) ->
         let* (ais: ai_state) = SM.get () in
         
         (* Get C(ppₜ) *)
         let aclos_ppt = PPMap.find_default ppt AClosSet.empty ais.c in

         (* C[ppₜ → C(ppₜ) ⊔ v] where v is the set of tuples of type clos *)
         let aclos' =
           AClosSet.union aclos aclos_ppt in

         let* () = SM.put { ais with c = PPMap.add ppt aclos' ais.c } in
         SM.return v
      | _ -> failwith "update_out: args do not match"
    else
      SM.return v

  (* [lift_tuple tl f ais] applies [f] to all the elements of [tl]
     and makes the union of all returned results *)
  let lift_tuple (tl: abst_value list list) (f: abst_value list -> (ai_state, abst_value) SM.t): (ai_state, abst_value) SM.t =
    SM.foldm_list (fun (av: abst_value) (t: abst_value list) ->
        let* av' = f t in
        join_f join_base_value av av'
      ) Bot tl

  (* Unspec functions *)
  (* getEnv: (ident, env) -> clos *)
  let getEnv (vl: abst_value list): (ai_state, abst_value) SM.t =
    match vl with
    | Tuple tl :: [] ->
       lift_tuple tl
         (fun t ->
           match t with
           | (Base (Ident(v)) :: (Base Env (st)) :: []) ->
              let aclos = List.assoc v st in
              SM.return (Base (AClos aclos))
           | _ -> failwith "getEnv: expected tuple of the form ident * env"
         ) 
    | _ -> failwith "getEnv: wrong number of arguments or wrong types"

  (* extEnv: (env, ident, clos) -> env*)
  let extEnv (vl: abst_value list): (ai_state, abst_value) SM.t =
      match vl with
      | Tuple (tl) :: [] ->
         lift_tuple tl
           (fun t ->
             match t with
             | (Base (Env (st)) :: Base (Ident v) :: Base (AClos aclos) :: []) ->
                let new_st = (v, aclos) :: (List.remove_assoc v st) in
                SM.return @@ Base (Env new_st)
             | _ ->
                failwith "extEnv: expected tuple of the form env * ident * clos"
           )
      | _ -> failwith "extEnv: wrong number of arguments or wrong types"

  (* mkClos: (ident, lterm, env) -> clos *)
  let mkClos (vl: abst_value list): (ai_state, abst_value) SM.t =
      match vl with
      | Tuple ([[Base (Ident _); PPoint pp_t; Base (Env env)]]) :: [] ->
         let* (ais: ai_state) = SM.get () in

         let pp = PPoint.parent pp_t in
         let env_pp = PPMap.find pp ais.rho in

         let new_env = LambdaUnspec.join_state env_pp env in
         let ais = { ais with rho = PPMap.add pp new_env ais.rho } in
         let* () = SM.put ais in

         SM.return @@ Base (AClos (AClosSet.singleton pp))
      | _ -> failwith "mkClos: expected one tuple representing a closure"


  (* getClos : clos -> (ident, lterm, env) *)
  let getClos (vl: abst_value list): (ai_state, abst_value) SM.t =
      match vl with
      | Base (AClos aclos) :: [] ->
         let* (ais: ai_state) = SM.get () in

         let ppoint_list = AClosSet.elements aclos in

         let ppoint_to_clos (pp: ppoint): abst_value list =
           let env = PPMap.find pp ais.rho in
           let lambda_abst = unfold pp_type_set pp ais.prg in
           match lambda_abst with
           | Const(_, Tuple [[x; lterm]]) ->
              [x; lterm; Base (Env env)]
           | _ ->
              failwith "getClos: expected a lambda-abstraction"
         in

         let clos_list = List.map ppoint_to_clos ppoint_list in
         SM.return @@ Tuple clos_list

      | _ -> failwith "getClos: expected one tuple representing a closure"

  (* Unspec vals and funs *)
  let unspec_funs = SMap.empty
  let unspec_funs = SMap.add "getEnv" getEnv unspec_funs
  let unspec_funs = SMap.add "extEnv" extEnv unspec_funs
  let unspec_funs = SMap.add "mkClos" mkClos unspec_funs
  let unspec_funs = SMap.add "getClos" getClos unspec_funs

  let unspec_vals = SMap.empty
  let unspec_vals = SMap.add "getEnv" (Fun([UnspecFun(1, [], "getEnv")])) unspec_vals
  let unspec_vals = SMap.add "extEnv" (Fun([UnspecFun(1, [], "extEnv")])) unspec_vals
  let unspec_vals = SMap.add "mkClos" (Fun([UnspecFun(1, [], "mkClos")])) unspec_vals
  let unspec_vals = SMap.add "getClos" (Fun([UnspecFun(1, [], "getClos")])) unspec_vals
  let unspec_vals = SMap.add "empty_env" (Base (Env([]))) unspec_vals
  let unspec_vals = SMap.add "x" (Base (Ident("x"))) unspec_vals
  let unspec_vals = SMap.add "y" (Base (Ident("y"))) unspec_vals
  let unspec_vals = SMap.add "z" (Base (Ident("z"))) unspec_vals
  let unspec_vals = SMap.add "a" (Base (Ident("a"))) unspec_vals
  let unspec_vals = SMap.add "b" (Base (Ident("b"))) unspec_vals
  let unspec_vals = SMap.add "f" (Base (Ident("f"))) unspec_vals
  let unspec_vals = SMap.add "g" (Base (Ident("g"))) unspec_vals

end
