type ident
type lit
type store
type int
type boolean

type value =
| Int int | Bool boolean

type expr =
| Const lit
| Var ident
| Plus (expr, expr)
| Equal (expr, expr)
| Less(expr, expr)
| Rand (lit, lit)

type stmt =
| Skip
| Assign (ident, expr)
| Seq (stmt, stmt)
| If (expr, stmt, stmt)
| While (expr, stmt)
| For (ident, expr, expr, stmt)

val litToVal : lit → value
val read : ident -> store → value
val write : ident -> store -> value → store
val add : int -> int → int
val inc: int -> int
val eq : int -> int → boolean
val lt : int -> int -> boolean
val le : int -> int -> boolean
val rand : lit -> lit → int
val isTrue : boolean → ()
val isFalse : boolean → ()

val eval_expr (s:store) (e:expr): value =
branch
	let Const i = e in
	litToVal i
or
	let Var x = e in
	read x s
or
	let Plus (e1, e2) = e in
	let Int v1 = eval_expr s e1 in
	let Int v2 = eval_expr s e2 in
	let v = add v1 v2 in
	Int v
or
	let Equal (e1, e2) = e in
	let Int v1 = eval_expr s e1 in
	let Int v2 = eval_expr s e2 in
	let v = eq v1 v2 in
	Bool v
or
	let Less(e1, e2) = e in
	let Int v1 = eval_expr s e1 in
	let Int v2 = eval_expr s e2 in
	let v = lt v1 v2 in
	Bool v
or
	let Rand (i1, i2) = e in
  let v = rand i1 i2 in
  Int v
end

val eval_stmt (s:store) (t:stmt): store =
branch
	let Skip = t in
	s
or
	let Assign (x, e) = t in
	let w = eval_expr s e in
	write x s w
or
	let Seq (t1, t2) = t in
	let s' = eval_stmt s t1 in
	eval_stmt s' t2
or
	let If (cond, true, false) = t in
	let Bool b = eval_expr s cond in
	branch
		let () = isTrue b in
		eval_stmt s true
	or
		let () = isFalse b in
		eval_stmt s false
	end
or
	let While (cond, t') = t in
	let Bool b = eval_expr s cond in
	branch
		let () = isTrue b in
		let s' = eval_stmt s t' in
		eval_stmt s' t
	or
		let () = isFalse b in
		s
	end
or
let For (var, init, fin, body) = t in
    let Int vinit = eval_expr s init in
    let Int vfin = eval_expr s fin in
    eval_for s var vinit vfin body
end


val eval_for (s: store) (x: ident) (init: int) (fin: int) (body: stmt): store =
  let b = le init fin in
  branch
let () = isTrue b in
    let s' = write x s (Int init) in
    let s'' = eval_stmt s' body in
    let init' = inc init in
    eval_for s'' x init' fin body
or
let () = isFalse b in
    s
end


val empty_store: store
val x: ident
val y: ident
val z: ident
val zero: lit
val mone: lit
val one: lit
val two: lit
val three: lit

val eval_empty_expr (e: expr): value = eval_expr empty_store e
val eval_empty_stmt (s: stmt): store = eval_stmt empty_store s

(* x := 1; x := 2 *)
val main1 : stmt =
  (Seq(Assign(x, Const one), Assign(x, Const two)))

(* x := 0; if (x = 0) { y := 1 } else { y := 2 } *)
val main2: stmt =
  (Seq(Assign(x, Const zero),
       If(Equal(Var x, Const zero),
          Assign(y, Const one),
          Assign(y, Const two))))

(* x := rand(0, 3); if (x = 0) { y := 1 } else { y := 2 } *)
val main3: stmt =
  (Seq(Assign(x, Rand(zero, three)),
       If(Equal(Var x, Const zero),
          Assign(y, Const one),
          Assign(y, Const two))))

(* x := 0; if (x < 1) { y := 1 } else { y := 2 } *)
val main4: stmt =
  (Seq(Assign(x, Const zero),
       If(Less(Var x, Const one), Assign(y, Const one), Assign(y, Const two))))

(* x := 0; while (x < 3) { x := x + 1 } *)
val main5: stmt =
  (Seq(Assign(x, Const zero),
       While(Less(Var x, Const three), Assign(x, Plus(Var x, Const one)))))

(* x := 0; y := 0; z := 0; while (x < 3) { y := 0; while (y < 2) {z := z + 1; y = y + 1}; x := x + 1} *)
val main6: stmt =
  (Seq(Assign(x, Const zero),
       Seq(Assign(y, Const zero),
           Seq(Assign(z, Const zero),
               While(Less(Var x, Const three),
                     Seq(Assign(y, Const zero),
                         Seq(While(Less(Var y, Const two),
                                   Seq(Assign(z, Plus(Var z, Const one)), Assign(y, Plus(Var y, Const one)))),
                             Assign(x, Plus(Var x, Const one)))))))))

(* x := 0; if rand(0, 3) < rand(0, 3) then x := x + 1 else x := x - 1; skip *)
val main7: stmt =
  (Seq((Seq(Assign(x, Const zero),
            If(Less(Rand(one, three), Rand(one, three)), Assign(x, Plus(Var x, Const one)), Assign(x, Plus(Var x, Const mone))))), Skip))

(* x := 2; y := 1;
   if (x + rand(-1, 1) == y) then
     while (x < y + 2) { x = x + 1 }
   else
      y = x + y;
   skip; *)
val main8: stmt =
  (Seq(Assign(x, Const two),
       Seq(Assign(y, Const one),
           If(Equal(Plus(Var x, Rand(mone, one)), Var y),
              While(Less(Var x, Plus(Var y, Const two)), Assign(x, Plus(Var x, Const one))),
              Assign(y, Plus(Var x, Var y))))))

(* x := 0; y := 3;
   while (x < y + 3) {
     if (x + rand(-1, 3) == y ) then
       y := y + 1;
     else
       x := x + 1
     x := x + 1
   } *)
val main9: stmt =
  (Seq(Seq(Assign(x, Const zero), Assign(y, Const three)),
       While(Less(Var x, Plus(Var y, Const three)),
             Seq(If(Equal(Plus(Var x, Rand(mone, three)), Var y),
                    Assign(y, Plus(Var y, Const one)),
                    Assign(x, Plus(Var x, Const one))),
                 Assign(x, Plus(Var x, Const one))))))

(* x := 0;
   while (x < 3) {
     x := x + rand(-1, 1)
   } *)
val main10: stmt =
  (Seq(Assign(x, Const zero),
       While(Less(Var x, Const three),
             Assign(x, Plus(Var x, Rand(mone, one))))))

(* x := 3;
   y := 1 - 1;
   if (y == 0) {
     while (y < x) {
       y = y + 2;
     }
   }
   else {
     y := 0;
   } *)
val main11: stmt =
  (Seq(Seq(Assign(x, Const three), Assign(y, Plus(Const one, Const mone))),
       If(Equal(Var y, Const zero),
          While(Less(Var y, Var x),
                Assign(y, Plus(Var y, Const two))),
          Assign(y, Const mone))))

(* x := 0;
   while (x == 0) {
     x := 0;
     x := x + 1;
   }
*)
val main12: stmt=
  (Seq(Assign(x, Const zero),
       (While(Equal(Var x, Const zero),
              Seq(Assign(x, Const zero),
                  Assign(x, Plus(Var x, Const one)))))))

(* 
   y := 0;
   for (x = 0; x < 3; x++) {
     y++
   }
*)
val main13: stmt=
  (Seq(Assign(y, Const zero),For(x, Const zero, Const three, Assign(y, Plus(Var y, Const one)))))

(* Tests unfold *)

val unfold1(_: ()): stmt =
  (Seq(Assign(x, Const zero),
       While(Less(Var x, Const three), Assign(x, Plus(Var x, Const one)))))

val unfold2(_: ()): stmt =
  (Seq(Assign(y, Plus(Const one, Const one)),
       (Seq(Assign(x, Const zero),
            While(Less(Var x, Const three), Assign(x, Plus(Var x, Const one)))))))
