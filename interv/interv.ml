include Interval_base.I

let interv (n1: int) (n2: int): t = v (Float.of_int n1) (Float.of_int n2)
let ( +! ) (i1: t) (i2: t): t = i1 + i2
let ( -! ) (i1: t) (i2: t): t = i1 - i2
let ( *! ) (i1: t) (i2: t): t = i1 * i2
let ( /! ) (i1: t) (i2: t): t = i1 / i2
let ( =! ) (i1: t) (i2: t): bool = (i1 = i2)
let ( = ) = Stdlib.( = )
let ( < ) = Stdlib.( < )
let ( > ) = Stdlib.( > )
let ( <= ) = Stdlib.( <= )
let ( >= ) = Stdlib.( >= )
