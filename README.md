# The Abstract Interpreter Generator (TAIGa)

Taiga generates abstract interpreters from [Skeletal Semantics](https://hal.inria.fr/hal-01881863v3).

## Build
The main dependencies for Taiga are [Dune](https://dune.build) and [Necrolib](https://gitlab.inria.fr/skeletons/necro) version 0.8, make sure you have them installed. Moreover, Taiga uses `dune-site` and `interval_base` library both available on opam.

Once all dependencies are installed, `dune build` should be all that is needed to compile Taiga.

### Step by step guide
The easiest way to build Taiga is to use the OCaml package manager: opam.
Once opam is installed, a switch should be created and the dependencies installed:
```
opam switch create name-of-switch ocaml-base-compiler.4.14.0
opam repository add necro https://gitlab.inria.fr/skeletons/opam-repository.git#necro
opam install dune dune-site necrolib.0.8 interval_base.1.6
```

Now running `dune build` should be all that is needed to build Taiga. The test files are located in `./tests/` and can be run using `dune exec -- tests/name-of-test.exe`.

Example: the `./tests/tests_lambda.ml` file contains the code to perform a CFA analysis for some lambda-calculus terms, and can be run with the command `dune exec -- tests/tests_lambda.exe`.

## How it works?
`lib/unspec.ml` defines two important signatures: `UNSPEC` and `TYPES`.
A module of type `UNSPEC` contains the definition of the abstract state,
the definitions of unspecified types, and comparisons and unions for these types.
`lib/types.ml` contains a functor building a `TYPES` module from an `UNSPEC` module.
A `TYPES` module contains the definitions of the `UNSPEC` module and the definitions of specified types,
along with their comparisons and unions.
To get a working interpreter, it remains to define the unspecified values in the `TYPES` module,
the update functions can also be given.
Finally `lib/absint.ml` defines a functor `AbstractInterp` which produces an abstract interpreter.

Examples of definitions of `UNSPEC` and `TYPES` modules are given in `examples`.
The `tests` folder contains some analysis performed using the abstract interpreters defined previously.
Tests can be run using `dune test`
