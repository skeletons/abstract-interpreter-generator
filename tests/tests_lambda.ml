open Lambda
module AI = Abstract.Abstint.AbstractInterp(Lambda)
module SMap = AI.SMap

let () = Printexc.record_backtrace true

let empty_e_meta = []
let eval_empty_lambda =
  let (_, _, _, eval_empty_opt) = SMap.find "eval_empty" AI.ss.ss_terms in
  Option.get eval_empty_opt

let abstract_int e s main_av =
  AI.abstract_interpretation_skeleton [] e s (Lambda.init_ai_state main_av)

let compute vname =
  let (_, _, _, main_opt)= SMap.find vname AI.ss.ss_terms in
  let main_t = Option.get main_opt in
  let main_av = AI.abstract_interpretation_term empty_e_meta main_t in
  let ais, v = abstract_int [empty_e_meta] (Apply(eval_empty_lambda, [main_t])) main_av in
  Printf.sprintf "%s: %s\n%s\n" vname (AI.av_to_string ais v) (AI.string_of_ai_state ais)

let _ =
  try
    begin
      Printf.printf "%s" (String.concat "\n" (List.map compute ["main1"; "main2"; "main3"; "main4"; "main5"; "main6"; "main7"]))
    end
  with e -> Printexc.print_backtrace stdout;
    Printf.printf "%s\n" (Printexc.to_string e)
